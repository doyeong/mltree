# Run arguments file auto-generated on Mon Feb 18 12:08:00 2019 by:
# JobTransform: RAWtoESD
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'ESDtoAOD' 

##jobproperties.Global.DetDescrVersion="ATLAS-R2-2016-01-00-01" # For MC16
##runArgs.conditionsTag = 'CONDBR2-BLKPA-2018-03'
runArgs.autoConfiguration = ['everything']
runArgs.maxEvents = -1
##runArgs.AMITag = 'q431'
##runArgs.geometryVersion = 'ATLAS-R2-2016-01-00-01'

# Input data
iflist = open("filecatalogue.dat","r")
runArgs.inputESDFile = [ l[:-1] for l in iflist.readlines() ]
#runArgs.inputESDFile = ['./ESD.pool.root']
#runArgs.inputESDFileType = 'ESD'
## runArgs.inputBSFileNentries = 1571
## runArgs.BSFileIO = 'input'
## 
# Output data
## runArgs.outputAODFile     = 'JZ1W.AOD.pool.root'
## runArgs.outputAODFileType = 'AOD'

# Extra runargs

# Extra runtime runargs

runArgs.preExec=["rec.doInDet=False;rec.doTrigger=False;rec.doMuon.set_Value_and_Lock(False);rec.doEgamma=False;rec.doTau=False;jetFlags.Enabled=False;rec.doMuonCombined=False;rec.doZdc=False;rec.doAlfa=False;recAlgs.doEFlow=False;recAlgs.doMissingET=False;recAlgs.doTrackParticleCellAssociation=False;rec.doTruth=True;"]


# Literal runargs snippets
runArgs.postInclude=["../athena/MLTree/python/CellTreeMaker.AllClusters.py"] 
