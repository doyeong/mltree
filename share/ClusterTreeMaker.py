#! /usr/bin/env python

from AthenaCommon.SystemOfUnits import GeV

from RecExConfig.ObjKeyStore import ObjKeyStore, objKeyStore
oks = ObjKeyStore()
oks.addStreamESD("CaloCellContainer", ["AllCalo"] )

### #Rerun topoclusters to produce calib hit moments
### from CaloRec.CaloTopoClusterFlags import jobproperties
### jobproperties.CaloTopoClusterFlags.doCalibHitMoments=True
### from CaloRec.CaloClusterTopoGetter import CaloClusterTopoGetter
### CaloClusterTopoGetter()

from AthenaCommon.AlgSequence import AlgSequence
### topSequence = AlgSequence()
### from CaloRec.CaloClusterTopoGetter import CaloClusterTopoGetter
### CaloClusterTopoGetter()
### topSequence.CaloTopoCluster.TopoCalibMoments.MomentsNames += ["ENG_CALIB_TOT","ENG_CALIB_OUT_T","ENG_CALIB_DEAD_TOT"]
### topSequence.CaloTopoCluster.TopoCalibMoments.DMCalibrationHitContainerNames = ["LArCalibrationHitDeadMaterial_DEAD","TileCalibHitDeadMaterial"]

### Jet finding
### from JetRecConfig.JetRecConfig import JetRecCfg, jetlog


#add MLTreeMaker directly to top sequence to ensure its run *after* topoclustering
from MLTree.MLTreeLibConf import ClusterTreeMaker
topSequence += ClusterTreeMaker(name = "JetClusterTreeMaker",
                                CaloClusterContainer   = "CaloCalTopoClusters",
                                JetContainer           = "AntiKt4LCTopoJets",
                                TruthJetContainer      = "AntiKt4TruthJets",
                                Prefix                 = "CALO",
                                ClusterEmin            = 0.0*GeV,
                                ClusterEmax            = 2000.0*GeV,
                                ClusterTruthEmin       = 300.*MeV,
                                ClusterTruthEmax       = 2000.0*GeV,
                                ClusterRapMin          = -5.0,
                                ClusterRapMax          =  5.0,
                                UncalibratedClusters   = False,
                                UseLCWScaleAsReference = False,
                                UseSignalStates        = True,
                                JetPtMin               = 20.0*GeV,
                                JetRapMin              = -2.0,
                                JetRapMax              =  2.0,
                                JetMatchingRadius      =  0.4,
                                UseSingleParticles     = False,
                                UseJets                = True,
                                DebugStreamFlag        = True,
                                DebugStreamFile        = "ClusterTreeMaker_debug_msg.dat",
                                MatchTruthJets         = True, 
                                OutputLevel            = INFO,
                                RootStreamName         = "OutputStream",
                                CaloClusterMomentNames = [
                                    "CENTER_MAG"    , "FIRST_PHI"     , "FIRST_ETA"      , "SECOND_R"         , "SECOND_LAMBDA"    , "DELTA_PHI"   , "DELTA_THETA"     , "DELTA_ALPHA"     ,      
                                    "CENTER_X"      , "CENTER_Y"      , "CENTER_Z"       , "CENTER_LAMBDA"    , "LATERAL"          , "LONGITUDINAL", "ENG_FRAC_EM"     , "ENG_FRAC_MAX"    ,      
                                    "ENG_FRAC_CORE" , "FIRST_ENG_DENS", "SECOND_ENG_DENS", "ISOLATION"        , "ENG_BAD_CELLS"    , "N_BAD_CELLS" , "N_BAD_CELLS_CORR", "BAD_CELLS_CORR_E", 
                                    "BADLARQ_FRAC"  , "ENG_POS"       , "SIGNIFICANCE"   , "CELL_SIGNIFICANCE", "CELL_SIG_SAMPLING", "AVG_LAR_Q"   , "AVG_TILE_Q"      , "ENG_BAD_HV_CELLS", 
                                    "N_BAD_HV_CELLS", "PTD"           , "MASS"           , "EM_PROBABILITY"   , "SECOND_TIME" 
                                ])

# Setup stream auditor
from AthenaCommon.AppMgr import ServiceMgr as svcMgr
if not hasattr(svcMgr, "DecisionSvc"):
    svcMgr += CfgMgr.DecisionSvc()
svcMgr.DecisionSvc.CalcStats = True
svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["OutputStream DATAFILE='jet.topo-cluster.pool.root' OPT='RECREATE'"]

# Setup up geometry needed for track extrapolation
### include("RecExCond/AllDet_detDescr.py")
### from AthenaCommon.CfgGetter import getService
### getService("AtlasTrackingGeometrySvc")
### 
# Configure object key store to recognize calo cells
