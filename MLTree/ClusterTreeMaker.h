// -*- c++ -*-
/**
 * @file    ClusterTreeMaker.h
 * @author  Joakim Olsson <joakim.olsson@cern.ch>
 * @author  Peter Loch <Peter.Loch@cern.ch>
 * @brief   Athena package to save cell images of clusters for ML training 
 * @date    October 2016
 */

#ifndef MLTREE_CLUSTERTREEMAKER_H
#define MLTREE_CLUSTERTREEMAKER_H

#include <string>
#include <map>
#include <vector>
#include <tuple>

#include <limits>

#include <fstream>

#include <TTree.h>

#include <GaudiKernel/SystemOfUnits.h>

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"

#include "StoreGate/ReadHandleKey.h"

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

#include "xAODTracking/VertexContainer.h"

///@brief Algorithm to produce flat @c ROOT tuple for ML studies
///
/// This algorithm stores al topo-cluster kinematics at EM and LCW scale (if available) for fully simulated (1) single particle 
/// or (2) jets. The output depends on the input, as some branches specific for particles are not filled and saved for jets and vice
/// versa. The topo-clusters are provided with the all reconstructed moments as well as relevant moments extracted from calibration 
/// hits. 
///
/// The algorithm is configured using various settings concerning the input and output modes, in addition object filters.  
class ClusterTreeMaker: public ::AthHistogramAlgorithm { 

public: 
  ///@brief Standard @c AthHistogramAlgorithm algorithm constructor
  ClusterTreeMaker( const std::string& name, ISvcLocator* pSvcLocator );
  ///@brief Default destructor
  virtual ~ClusterTreeMaker(); 

  ///@name Implementation of the @c AthHistogramAlgorithm interfaces
  ///@{
  virtual StatusCode  initialize(); ///< Initialization: setup
  virtual StatusCode  execute();    ///< Execution: filling the @c ROOT tuple and writing it
  virtual StatusCode  finalize();   ///< Finalization: summary
  ///@}

private: 

  ///@name Useful type
  ///@{
  ///
  /// Container type linking the @c xAOD::CaloCluster::MomentType with a human-readable moment type, a pointer to the storage location for the moment value 
  /// (the memory address registered with @c ROOT ), and a scale factor for the given moment - typically a measure for the unit.   
  typedef std::tuple<xAOD::CaloCluster::MomentType,std::string,float*,double>                                     MomentLink; //mlink_obj_t;
  /// List/vector of links
  typedef std::vector<MomentLink>                                                                                 MomentLinkVector; //mlink_cnt_t;
  /// Ranked list of paired pointers to clusters, sorted descending in cluster energy
  typedef std::multimap<float,std::tuple<const xAOD::CaloCluster*,const xAOD::CaloCluster*>,std::greater<float> > RankedClusterMap; // rankmap_t;
  /// A container for pointers to non-modifiable @c xAOD::CaloCluster typed objects 
  typedef std::vector<const xAOD::CaloCluster*>                                                                   ClusterVector; //cvect_t;
  /// A container storing two linked jets (typially a generated truth and a simulated reco jet) together with their angular distance 
  typedef std::tuple<const xAOD::Jet*,const xAOD::Jet*,double>                                                    MatchedJetPair;       //match_obj_t;
  /// List/vector of macthed jets
  typedef std::vector<MatchedJetPair>                                                                             MatchedJetPairVector; //match_cnt_t;
  /// Container for a pointer to the matched (truth) jet and the matching radius
  typedef std::tuple<const xAOD::Jet*,double>                                                                     MatchedJet;           //match_entry_t;
  // A 1-to-many lookup to retrieve all truth jets and their respective distances (payload) associated with a reco jet (pointer to this jet is the key) 
  typedef std::multimap<const xAOD::Jet*,MatchedJet>                                                              RankedMatchedJetMap;   //match_map_t;
  ///@}

  bool               m_debugStreamFlag  = { false           };
  std::string        m_debugStreamName  = { ""              };
  std::ofstream      m_debugStream      = { std::ofstream() };     

  void debugMessage   (const std::string& module,const std::string& fmtStr,...); 
  void resourceMessage(const std::string& module,const std::string& tagStr    );

  ////////////////
  // Properties //
  ////////////////

  // -- input (read) handles
  SG::ReadHandleKey<xAOD::CaloClusterContainer>   m_clusterContainerKey       = { "CaloCalTopoClusters" };  
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_truthParticleContainerKey = { "TruthParticles"      };
  SG::ReadHandleKey<xAOD::EventInfo>              m_eventInfoKey              = { "EventInfo"           }; 
  SG::ReadHandleKey<xAOD::JetContainer>           m_jetContainerKey           = { "AntiKt4LCTopoJets"   };
  SG::ReadHandleKey<xAOD::JetContainer>           m_truthJetContainerKey      = { "AntiKt4TruthJets"    };
  SG::ReadHandleKey<xAOD::VertexContainer>        m_vertexContainerKey        = { "PrimaryVertices"     };

  // -- container keys and such
  std::string m_prefix                   = { ""                    };

  // -- topo-cluster selection
  double m_clusterEmin                   = { 0.                      };
  double m_clusterEmax                   = { 2000.*Gaudi::Units::GeV };
  double m_clusterRapMin                 = { -5.0   };
  double m_clusterRapMax                 = {  5.0   };

  // -- topo-cluster truth selection
  double m_clusterTruthEmin              = { 0.                      };
  double m_clusterTruthEmax              = { 2000.*Gaudi::Units::GeV };

  // -- jet selection
  double m_jetPtMin                      = { 20.*Gaudi::Units::GeV };
  double m_jetRapMin                     = { -4.5   };
  double m_jetRapMax                     = {  4.5   }; 
  double m_jetMatchRadius                = {  0.4   };

  // -- configuration
  bool   m_doUnCalibratedClusters        = { true   };
  bool   m_useSignalStates               = { false  };
  bool   m_useLCWScaleAsReference        = { false  };
  bool   m_useJets                       = { false  };
  bool   m_matchTruthJets                = { true   };
  bool   m_useSingleParticles            = { true   };

  // -- scale controls
  bool  m_calibratedConstituents              = { true                          };
  xAOD::CaloCluster::State m_constituentScale = { xAOD::CaloCluster::CALIBRATED };

  // -- moments to be included into the tuple
  std::vector<std::string> m_caloClusterMoments;

  ////////////////////////////
  // Internal stores/caches //
  ////////////////////////////

  // -- cache links requested moment with address in tuple
  MomentLinkVector m_caloClusterMomentLink;

  // -- internal event/cluster counters
  int m_allEvntCtr = { 0 };
  int m_accEvntCtr = { 0 };
  int m_allClusCtr = { 0 };
  int m_accClusCtr = { 0 };

  /////////////////////
  // Tuple structure //
  /////////////////////  

  // -- tree
  TTree* m_clusterTree = { (TTree*)0 };

  // -- event info
  int       m_runNumber   = { 0  };
  long long m_eventNumber = { 0  };
  long long m_sequence    = { 0  };
  float     m_averageMu   = { 0. };
  int       m_nPrimVtx    = { 0  };

  ///@name Particle truth 
  ///@{
  long long m_fParticleCount   = { 0  };
  float     m_fClusterTruthE   = { 0. };
  float     m_fClusterTruthPt  = { 0. };
  float     m_fClusterTruthEta = { 0. };
  float     m_fClusterTruthPhi = { 0. };
  int       m_fClusterTruthPDG = { 0  };
  ///@}

  ///@name Reconstructed jet data
  ///@{
  long long m_fJetCount        = { 0  }; ///< Number of jet in event (arbitrary order?)     
  float     m_fJetCalE         = { 0. }; ///< Calibrated (JES scale) jet energy		    
  float     m_fJetCalPt        = { 0. }; ///< Calibrated (JES scale) jet transverse momentum
  float     m_fJetCalRap       = { 0. }; ///< Calibrated (JES scale) jet rapidity
  float     m_fJetCalPhi       = { 0. }; ///< Calibrated (JES scale) jet azimuth
  int       m_fJetNConst       = { 0  }; ///< Number of constituents
  float     m_fJetRawE         = { 0. }; ///< Raw (constituent scale) jet energy             		    
  float     m_fJetRawPt        = { 0. }; ///< Raw (constituent scale) jet transverse momentum
  float     m_fJetRawRap       = { 0. }; ///< Raw (constituent scale) jet rapidity           
  float     m_fJetRawPhi       = { 0. }; ///< Raw (constituent scale) jet azimuth            
  ///@}
  ///@name Truth jet data
  ///@{
  float m_fTruthJetMatchRadius  = { 0. }; ///< Matching distance to reconstructed jet 
  float m_fTruthJetE            = { 0. }; ///< Truth jet energy             
  float m_fTruthJetPt           = { 0. }; ///< Truth jet transverse momentum
  float m_fTruthJetRap          = { 0. }; ///< Truth jet rapidity           
  float m_fTruthJetPhi          = { 0. }; ///< Truth jet azimuth            
  ///@}

  // -- cluster specs
  int   m_nCluster            = { 0 };
  int   m_fCluster_nCells     = { 0 }; // # cells with E > 0
  int   m_fCluster_nCells_tot = { 0 }; // # all cells
  int   m_fClusterIndex       = { 0 };

  // -- cluster signals: E0 scale (optional use)
  float m_fClusterE          = { 0. };
  float m_fClusterPt         = { 0. };
  float m_fClusterEta        = { 0. };
  float m_fClusterPhi        = { 0. };
  float m_fCluster_sumCellE  = { 0. };
  float m_fClusterTime       = { 0. };
  float m_fCluster_fracE     = { 0. }; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  float m_fCluster_fracE_ref = { 0. }; // fractional contribution particle (cluster_energy/reference energy)   
  
  // -- cluster signals: LCW scale (default/always use)
  float m_fClusterECalib          = { 0. };
  float m_fClusterPtCalib         = { 0. };
  float m_fClusterEtaCalib        = { 0. };
  float m_fClusterPhiCalib        = { 0. };
  float m_fCluster_sumCellECalib  = { 0. };
  float m_fCluster_fracECalib     = { 0. }; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  float m_fCluster_fracECalib_ref = { 0. }; // fractional contribution particle (cluster_energyreference energy)   

  // -- cluster truth info
  float m_fCluster_ENG_CALIB_TOT       = { 0. };  // energy deposit in cluster cells 
  float m_fCluster_ENG_CALIB_OUT_L     = { 0. };  // energy deposit outside of cluster but associated (loose selection)
  float m_fCluster_ENG_CALIB_OUT_M     = { 0. };  // energy deposit outside of cluster but associated (medium selection) 
  float m_fCluster_ENG_CALIB_OUT_T     = { 0. };  // energy deposit outside of cluster but associated (tight selection)
  float m_fCluster_ENG_CALIB_DEAD_L    = { 0. };  // energy deposit in dead material associated with the cluster (loose selection)  
  float m_fCluster_ENG_CALIB_DEAD_M    = { 0. };  // energy deposit in dead material associated with the cluster (medium selection)
  float m_fCluster_ENG_CALIB_DEAD_T    = { 0. };  // energy deposit in dead material associated with the cluster (tight selection)
  float m_fCluster_ENG_CALIB_DEAD_TOT  = { 0. };  // energy deposit in dead material associated with the cluster (all losses)
  float m_fCluster_ENG_CALIB_FRAC_EM   = { 0. };  // energy deposited by e/gamma inside the cluster
  float m_fCluster_ENG_CALIB_FRAC_HAD  = { 0. };  // energy deposited by charged pions inside the cluster
  float m_fCluster_ENG_CALIB_FRAC_REST = { 0. };  // energy deposited by all other particles in the cluster 

  /// -- calibration weights
  float m_fCluster_EM_PROBABILITY = { 0. };       // EM likelihood
  float m_fCluster_HAD_WEIGHT     = { 0. };       // hadronic weights sum(w_cell x E_cell)/sum(E_cell)
  float m_fCluster_OOC_WEIGHT     = { 0. };       // out-of-cluster correction factor (OOC)
  float m_fCluster_DM_WEIGHT      = { 0. };       // dead material correction factor (DMC)
  float m_fCluster_Ehad           = { 0. };       // hadronic calibration applied:  HAD_WEIGHT * ClusterE
  float m_fCluster_Eooc           = { 0. };       // amount of energy added by OOC: (1-OOC_WEIGHT)*Ehad 
  float m_fCluster_Edmc           = { 0. };       // amount of energy added by DMC: (1-DM_WEIGHT)*(Ehad+Eooc)
 
  // -- cluster moments (used as configured)
  float m_fCluster_CENTER_MAG        = { 0. };    // always used (historic)
  float m_fCluster_FIRST_PHI         = { 0. };
  float m_fCluster_FIRST_ETA         = { 0. };
  float m_fCluster_SECOND_R          = { 0. };
  float m_fCluster_SECOND_LAMBDA     = { 0. };
  float m_fCluster_DELTA_PHI         = { 0. };
  float m_fCluster_DELTA_THETA       = { 0. };
  float m_fCluster_DELTA_ALPHA       = { 0. };
  float m_fCluster_CENTER_X          = { 0. };
  float m_fCluster_CENTER_Y          = { 0. };
  float m_fCluster_CENTER_Z          = { 0. };
  float m_fCluster_CENTER_LAMBDA     = { 0. };
  float m_fCluster_LATERAL           = { 0. };
  float m_fCluster_LONGITUDINAL      = { 0. };
  float m_fCluster_ENG_FRAC_EM       = { 0. };
  float m_fCluster_ENG_FRAC_MAX      = { 0. };
  float m_fCluster_ENG_FRAC_CORE     = { 0. };
  float m_fCluster_ENG_FRAC_TIME     = { 0. };
  float m_fCluster_CELL_TIME_MAX     = { 0. };
  float m_fCluster_FIRST_ENG_DENS    = { 0. };    // always used (historic)
  float m_fCluster_SECOND_ENG_DENS   = { 0. };
  float m_fCluster_ISOLATION         = { 0. };
  float m_fCluster_ENG_BAD_CELLS     = { 0. };
  float m_fCluster_N_BAD_CELLS       = { 0. };
  float m_fCluster_N_BAD_CELLS_CORR  = { 0. };
  float m_fCluster_BAD_CELLS_CORR_E  = { 0. };
  float m_fCluster_BADLARQ_FRAC      = { 0. };
  float m_fCluster_ENG_POS           = { 0. };
  float m_fCluster_SIGNIFICANCE      = { 0. };
  float m_fCluster_CELL_SIGNIFICANCE = { 0. };
  float m_fCluster_CELL_SIG_SAMPLING = { 0. };
  float m_fCluster_AVG_LAR_Q         = { 0. };
  float m_fCluster_AVG_TILE_Q        = { 0. };
  float m_fCluster_ENG_BAD_HV_CELLS  = { 0. };
  float m_fCluster_N_BAD_HV_CELLS    = { 0. };
  float m_fCluster_PTD               = { 0. };
  float m_fCluster_MASS              = { 0. };
  float m_fCluster_SECOND_TIME       = { 0. };

  // -- control and constants
  static std::map<std::string,xAOD::CaloCluster::MomentType> m_knownClusterMomentType;
  static std::map<xAOD::CaloCluster::MomentType,std::string> m_knownClusterMomentName;
  static std::map<xAOD::CaloCluster::MomentType,double>      m_knownClusterMomentScale;

  /////////////
  // Helpers //
  /////////////

  // -- adding cluster moments to the tuple and managing their content
  bool        bookClusterMoments(size_t nwrds=0);             // book 
  void        registerClusterMoment(MomentLink& mlink);       // register moments in lookup table
  void        resetClusterMomentLinkValue(float value=0.);    // reset link values
  bool        resetAll();                                     // reset all variables
  bool        resetClusters();                                // reset cluster variables
  // -- select jets and clusters
  bool        applyFilter(const xAOD::CaloCluster& rclus,xAOD::CaloCluster::State s=xAOD::CaloCluster::UNCALIBRATED); // cluster filter
  bool        applyFilter(const xAOD::Jet& rjet);             // jet filter
  // -- find truth and reco jet matches 
  MatchedJetPairVector findMatches(const xAOD::JetContainer& recoJets,const xAOD::JetContainer& truthJets); 
  // -- fill data blocks
  bool fillTruthJetBlock(const xAOD::Jet* pjet,double matchRadius); 
  bool fillRecoJetBlock (const xAOD::Jet* pjet,xAOD::CaloCluster::State s=xAOD::CaloCluster::CALIBRATED);

  ///////////////
  // Templates //
  ///////////////

  // -- fill cluster data
  template<class CCONT> bool fillClusters(const CCONT* clusCont,double refEnergy,xAOD::CaloCluster::State s=xAOD::CaloCluster::UNCALIBRATED) {
    static const std::string mname = "ClusterTreeMaker::fillClusters(...)";
    // -- topo-cluster counter
    m_allClusCtr += (int)clusCont->size();
    // -- sort and filter
    RankedClusterMap rankedClusters; double eSum(0.); double eSumCalib(0.); 
    for ( size_t i(0); i<clusCont->size(); ++i ) { 
      const xAOD::CaloCluster* calClus = clusCont->at(i);                                                  // allocate cluster
      const xAOD::CaloCluster* rawClus = nullptr; 
      if ( m_doUnCalibratedClusters ) {                                                                    // old style provides geometrical cell weights
	rawClus = calClus->getSisterCluster();
	if ( rawClus == nullptr ) { 
	  ATH_MSG_ERROR("invalid null-pointer to sister cluster for cal cluster " << calClus->index());
	  return false; 
	}
	if ( calClus->index() != rawClus->index() ) { 
	  ATH_MSG_ERROR("index mismatch: raw/cal index " << rawClus->index() << "/" << calClus->index());
	  return false;
	} 
      } else {
	rawClus = calClus; 
      }
      // check scale preference and apply corresponding filter and sorting argument
      if ( applyFilter(*calClus,s) ) {
	eSum += calClus->e(xAOD::CaloCluster::UNCALIBRATED); eSumCalib += calClus->e(xAOD::CaloCluster::CALIBRATED);  // always uncalibrated/calibrated, independent of s 
	rankedClusters.emplace_hint(rankedClusters.end(),calClus->e(s)/Gaudi::Units::GeV,std::make_tuple(calClus,rawClus)); 
      }
    }
    // -- total number of surviving clusters
    m_nCluster = static_cast<int>(rankedClusters.size());
    // -- accepted event and cluster counters
    ++m_accEvntCtr; m_accClusCtr += m_nCluster;
    m_fClusterIndex = 0;
    if ( m_nCluster == 0 ) { 
      ++m_sequence; 
      debugMessage(mname,"# ----- [clusters] - checking: found %i ranked clusters only (failed)",m_nCluster); 
      return false; 
    }
    // -- loop over clusters in order of their energies
    bool isOk(true); 
    for(const auto& mpair : rankedClusters ) {
      // reset all previously filled data
      resetClusters(); debugMessage(mname,"# ----- [clusters] - reset all cluster variables %i/%i",m_fClusterIndex+1,m_nCluster);
      // allocate clusters
      const xAOD::CaloCluster* calClus = std::get<0>(mpair.second);
      const xAOD::CaloCluster* rawClus = std::get<1>(mpair.second);
      if ( rawClus == nullptr ) { ATH_MSG_ERROR( "unexpected invalid pointer to raw signal cluster raw = " << rawClus << " cal = " << calClus ); isOk = false; break; }
      // basic scale clusters
      if ( m_useSignalStates ) { 
	m_fClusterE          = calClus->e  (xAOD::CaloCluster::UNCALIBRATED)/Gaudi::Units::GeV       ;
	m_fClusterPt         = calClus->pt (xAOD::CaloCluster::UNCALIBRATED)/Gaudi::Units::GeV       ;
	m_fClusterEta        = calClus->eta(xAOD::CaloCluster::UNCALIBRATED)                         ;
	m_fClusterPhi        = calClus->phi(xAOD::CaloCluster::UNCALIBRATED)/Gaudi::Units::radian    ;
	m_fClusterTime       = calClus->time()                              /Gaudi::Units::nanosecond;
      } else { 
	m_fClusterE          = rawClus->e  (xAOD::CaloCluster::UNCALIBRATED) /Gaudi::Units::GeV   ;
	m_fClusterPt         = rawClus->pt (xAOD::CaloCluster::UNCALIBRATED) /Gaudi::Units::GeV   ;
	m_fClusterEta        = rawClus->eta(xAOD::CaloCluster::UNCALIBRATED)                      ;
	m_fClusterPhi        = rawClus->phi(xAOD::CaloCluster::UNCALIBRATED) /Gaudi::Units::radian;
	m_fClusterTime       = rawClus->time()/Gaudi::Units::nanosecond;
      }
      m_fCluster_fracE     = eSum != 0.      ? m_fClusterE/eSum      : 0.;
      m_fCluster_fracE_ref = refEnergy != 0. ? m_fClusterE/refEnergy : 0.;
      // LCW scale clusters
      m_fClusterECalib          = calClus->e  (xAOD::CaloCluster::CALIBRATED)/Gaudi::Units::GeV   ; 
      m_fClusterPtCalib         = calClus->pt (xAOD::CaloCluster::CALIBRATED)/Gaudi::Units::GeV   ;
      m_fClusterEtaCalib        = calClus->eta(xAOD::CaloCluster::CALIBRATED)                     ;
      m_fClusterPhiCalib        = calClus->phi(xAOD::CaloCluster::CALIBRATED)/Gaudi::Units::radian;
      m_fCluster_fracECalib     = eSumCalib != 0. ? m_fClusterECalib/eSumCalib : 0.;
      m_fCluster_fracECalib_ref = refEnergy != 0. ? m_fClusterECalib/refEnergy : 0.;
      // -- collect truth moments
      double mval(0.);
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_TOT,      mval) ) { m_fCluster_ENG_CALIB_TOT       = -1.; } else { m_fCluster_ENG_CALIB_TOT       = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_TOT      ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_T,    mval) ) { m_fCluster_ENG_CALIB_OUT_T     = -1.; } else { m_fCluster_ENG_CALIB_OUT_T     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_OUT_T    ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_L,    mval) ) { m_fCluster_ENG_CALIB_OUT_L     = -1.; } else { m_fCluster_ENG_CALIB_OUT_L     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_OUT_L    ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_M,    mval) ) { m_fCluster_ENG_CALIB_OUT_M     = -1.; } else { m_fCluster_ENG_CALIB_OUT_M     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_OUT_M    ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_TOT, mval) ) { m_fCluster_ENG_CALIB_DEAD_TOT  = -1.; } else { m_fCluster_ENG_CALIB_DEAD_TOT  = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_TOT ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_T,   mval) ) { m_fCluster_ENG_CALIB_DEAD_T    = -1.; } else { m_fCluster_ENG_CALIB_DEAD_T    = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_T   ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_L,   mval) ) { m_fCluster_ENG_CALIB_DEAD_L    = -1.; } else { m_fCluster_ENG_CALIB_DEAD_L    = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_L   ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_M,   mval) ) { m_fCluster_ENG_CALIB_DEAD_M    = -1.; } else { m_fCluster_ENG_CALIB_DEAD_M    = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_M   ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_FRAC_EM,  mval) ) { m_fCluster_ENG_CALIB_FRAC_EM   = -1.; } else { m_fCluster_ENG_CALIB_FRAC_EM   = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_FRAC_EM  ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_FRAC_HAD, mval) ) { m_fCluster_ENG_CALIB_FRAC_HAD  = -1.; } else { m_fCluster_ENG_CALIB_FRAC_HAD  = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_FRAC_HAD ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_FRAC_REST,mval) ) { m_fCluster_ENG_CALIB_FRAC_REST = -1.; } else { m_fCluster_ENG_CALIB_FRAC_REST = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_FRAC_REST); }
      // -- collect calibration factors and EM likelihood
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::HAD_WEIGHT,    mval) ) { m_fCluster_HAD_WEIGHT     = -1.; } else { m_fCluster_HAD_WEIGHT     = mval; } 
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::OOC_WEIGHT,    mval) ) { m_fCluster_OOC_WEIGHT     = -1.; } else { m_fCluster_OOC_WEIGHT     = mval; }
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::DM_WEIGHT,     mval) ) { m_fCluster_DM_WEIGHT      = -1.; } else { m_fCluster_DM_WEIGHT      = mval; }
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::EM_PROBABILITY,mval) ) { m_fCluster_EM_PROBABILITY = -1.; } else { m_fCluster_EM_PROBABILITY = mval; }
      // -- retrieve static moments (deprecated)
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::CENTER_MAG,    mval) ) { m_fCluster_CENTER_MAG     = -1.; } else { m_fCluster_CENTER_MAG     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::CENTER_MAG);     }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::FIRST_ENG_DENS,mval) ) { m_fCluster_FIRST_ENG_DENS = -1.; } else { m_fCluster_FIRST_ENG_DENS = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::FIRST_ENG_DENS); } 
      // -- dynamically scheduled reconstructed moments
      for ( auto& mlnk : m_caloClusterMomentLink ) {
	if ( !rawClus->retrieveMoment(std::get<0>(mlnk),mval) ) { *(std::get<2>(mlnk)) = -1.; } else { *(std::get<2>(mlnk)) = mval * std::get<3>(mlnk); } 
      }
      // -- cluster composition and nomimal sums
      m_fCluster_nCells        = 0;
      m_fCluster_nCells_tot    = 0;
      m_fCluster_sumCellE      = 0.;
      m_fCluster_sumCellECalib = 0.;
      // Note: if rawClus is sister cluster, cell energy weights do not include LCW!
      for ( auto fcell(rawClus->cell_begin()); fcell != rawClus->cell_end(); ++fcell ) { if ( (*fcell)->e() > 0. ) { m_fCluster_sumCellE += (*fcell)->e()*fcell.weight(); ++m_fCluster_nCells; } ++m_fCluster_nCells_tot; }
      if ( m_useSignalStates || rawClus == calClus ) {
	m_fCluster_sumCellE      /= Gaudi::Units::GeV; 
	m_fCluster_sumCellECalib  = m_fCluster_sumCellE;
	if ( m_fCluster_sumCellECalib != 0. ) { m_fCluster_sumCellE *= (m_fClusterE/m_fClusterECalib); }  // cell weights include LCW -> unddo total effect of LCW on topo-cluster from EM scale sum
      } else {
	for ( auto fcell(calClus->cell_begin()); fcell != calClus->cell_end(); ++fcell ) { if ( (*fcell)->e() > 0. ) { m_fCluster_sumCellECalib += (*fcell)->e()/Gaudi::Units::GeV*fcell.weight();                     }                         }
      }
      // fill tuple structure
      ++m_sequence; m_clusterTree->Fill(); ++m_fClusterIndex;
    } // loop on ranked clusters
    if ( !isOk ) { 
      debugMessage(mname,"# ----- [clusters] - checking: found invalid pointer to sister cluster (failed)"); 
    }
    return isOk;
  }
}; 

#endif
