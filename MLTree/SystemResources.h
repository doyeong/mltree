// -*- c++ -*-
#ifndef MLTREE_SYSTEMREOSURCES_H
#define MLTREE_SYSTEMREOSURCES_H

#include <string>
#include <map>
#include <tuple>

#include <unistd.h>

#include <cstdio>

#ifndef RESOURCE_STREAM
#define RESOURCE_STREAM "/proc/self/stat"
#endif

namespace Resource {
  ///
  enum class Unit { B = 0x00001, kB = 0x0010, MB = 0x00100, GB = 0x01000, TB = 0x10000, Unknown = 0x00000 };
  static const std::map<Unit,std::tuple<double,std::string> > unitDict = {
    { Unit::B      , { std::pow(2., 0.), "B"     } },
    { Unit::kB     , { std::pow(2.,10.), "kB"    } },
    { Unit::MB     , { std::pow(2.,20.), "MB"    } },
    { Unit::GB     , { std::pow(2.,40.), "GB"    } },
    { Unit::TB     , { std::pow(2.,80.), "TB"    } },
    { Unit::Unknown, { -1.             , "(unk)" } }                 
  };
  static double             unitSize (Unit u) { return std::get<0>(unitDict.at(u)); }
  static double             unitScale(Unit u) { return 1./unitSize(u)             ; } 
  static const std::string& unitName (Unit u) { return std::get<1>(unitDict.at(u)); }

  ///@brief Container for snapshot of selected system resources
  struct SnapShot {
    const long long psize          = getpagesize();
    bool               valid       = { false };
    int                pid         = { 0  }; ///< Process id (current process)
    std::string        comment     = { "" }; ///< Comment 
    std::string        state       = { "" }; ///< State
    int                ppid        = { 0  }; ///< Parent process id
    int                pgrp        = { 0  }; ///< Process group 
    int                session     = { 0  }; ///< Session id
    int                tty_r       = { 0  }; ///< Controlling terminal number
    int                tpgid       = { 0  }; ///< Forward group id of the controlling terminal
    unsigned int       flags       = { 0  }; ///< Kernel flags
    unsigned long      minflt      = { 0  }; ///< Number of minor faults not requiring loading a memory page from disc
    unsigned long      cminflt     = { 0  }; ///< Number of minor faults that the process's waited-for children have made
    unsigned long      maxflt      = { 0  }; ///< Number of major faults requiring loading a memory page from disc
    unsigned long      cmaxflt     = { 0  }; ///< Number of major faults that the process's waited-for children have made
    unsigned long      utime       = { 0  }; ///< Amount of time scheduled for this process in user mode
    unsigned long      stime       = { 0  }; ///< Amount of time scheduled for this process in kernel mode
    long int           cutime      = { 0  }; ///< Amount of time scheduled for the children of this process in user mode
    long int           cstime      = { 0  }; ///< Amount of time scheduled for the children of this process in kernel mode
    long int           priority    = { 0  }; ///< Priority indicator for this process
    long int           nice        = { 0  }; ///< Nice value of this process
    long int           nthreads    = { 0  }; ///< Number of threads in this process
    long int           itrealvalue = { 0  }; ///< Obsolete since Kernel 2.6 
    unsigned long long starttime   = { 0  }; ///< Start time of process after kernel reboot (in clock ticks)
    unsigned long      vsize       = { 0  }; ///< Virtual memory size in bytes
    unsigned int       rss         = { 0  }; ///< Resident set size (number of memory pages)
    double             pmemory     = { 0. }; ///< Used physical memory
    double             vmemory     = { 0. }; ///< Used virtual memory
    SnapShot() { }
    SnapShot(const SnapShot& s)
      : valid      (s.valid      ) 
      , pid        (s.pid        )
      , comment    (s.comment    )
      , state      (s.state      )
      , ppid       (s.ppid       )
      , pgrp       (s.pgrp       )
      , session    (s.session    )
      , tty_r      (s.tty_r      )
      , tpgid      (s.tpgid      )
      , flags      (s.flags      )
      , minflt     (s.minflt     )
      , cminflt    (s.cminflt    )
      , maxflt     (s.maxflt     )
      , cmaxflt    (s.cmaxflt    )
      , utime      (s.utime      )
      , stime      (s.stime      )
      , cutime     (s.cutime     )
      , cstime     (s.cstime     )
      , priority   (s.priority   )
      , nice       (s.nice       )
      , nthreads   (s.nthreads   )
      , itrealvalue(s.itrealvalue)
      , starttime  (s.starttime  )
      , vsize      (s.vsize      )
      , rss        (s.rss        )
      , pmemory    (s.pmemory    )
      , vmemory    (s.vmemory    )
    { }
    SnapShot& operator=(const SnapShot& s) { 
      if  ( &s == this ) { return *this; } 
      valid       = s.valid      ;
      pid         = s.pid        ;
      comment     = s.comment    ;
      state       = s.state      ;
      ppid        = s.ppid       ;
      pgrp        = s.pgrp       ;
      session     = s.session    ;
      tty_r       = s.tty_r      ;
      tpgid       = s.tpgid      ;
      flags       = s.flags      ;
      minflt      = s.minflt     ;
      cminflt     = s.cminflt    ;
      maxflt      = s.maxflt     ;
      cmaxflt     = s.cmaxflt    ;
      utime       = s.utime      ;
      stime       = s.stime      ;
      cutime      = s.cutime     ;
      cstime      = s.cstime     ;
      priority    = s.priority   ;
      nice        = s.nice       ;
      nthreads    = s.nthreads   ;
      itrealvalue = s.itrealvalue;
      starttime   = s.starttime  ;
      vsize       = s.vsize      ;
      rss         = s.rss        ;
      pmemory     = s.pmemory    ;
      vmemory     = s.vmemory    ;
      return *this; 
    }
  };
  ///@ brief generate a system snapshot
  static SnapShot takeSnapShot() { 
    SnapShot s;
    std::ifstream statStream( RESOURCE_STREAM,std::ios_base::in);
    if ( statStream.good() )  { 
      statStream >> 
	s.pid        >>
        s.comment    >>
        s.state      >>
        s.ppid       >>
        s.pgrp       >>
        s.session    >>
        s.tty_r      >>
        s.tpgid      >>
        s.flags      >>
        s.minflt     >>
        s.cminflt    >>
        s.maxflt     >>
        s.cmaxflt    >>
        s.utime      >>
        s.stime      >>
        s.cutime     >>
        s.cstime     >>
        s.priority   >>
        s.nice       >>
        s.nthreads   >>
        s.itrealvalue>>
        s.starttime  >>
        s.vsize      >>
        s.rss       ;   // other data is ignored
      s.pmemory = static_cast<double>(s.rss*s.psize);
      s.vmemory = static_cast<double>(s.vmemory)    ;
      s.valid   = true; 
    }
    return s;
  }
  namespace Value {
    static double physicalMemory(const SnapShot& s,Unit u=Unit::kB) { return s.pmemory*unitScale(u)          ; }
    static double virtualMemory (const SnapShot& s,Unit u=Unit::kB) { return s.vmemory*unitScale(u)          ; }
    static double physicalMemory(Unit u=Unit::kB)                   { return physicalMemory(takeSnapShot(),u); }
    static double virtualMemory (Unit u=Unit::kB)                   { return virtualMemory (takeSnapShot(),u); }
  }
  static std::string physicalMemory(Unit u=Unit::kB,bool addUnit=true,int ndigits=1) { 
    static char _buffer[64];
    if ( ndigits < 1 ) { return std::string(); }
    SnapShot s; 
    if ( addUnit ) { 
      sprintf(_buffer,"%.*f %s",ndigits,Value::physicalMemory(u),unitName(u).c_str());
    } else {
      sprintf(_buffer,"%.*f",ndigits,Value::physicalMemory(u));
    }
    return std::string(_buffer);
  }
  static std::string virtualMemory(Unit u=Unit::kB,bool addUnit=true,int ndigits=1) { 
    static char _buffer[64];
    if ( ndigits < 1 ) { return std::string(); }
    SnapShot s; 
    if ( addUnit ) { 
      sprintf(_buffer,"%.*f %s",ndigits,Value::virtualMemory(u),unitName(u).c_str());
    } else {
      sprintf(_buffer,"%.*f",ndigits,Value::virtualMemory(u));
    }
    return std::string(_buffer);
  }
} // Resource
#endif
