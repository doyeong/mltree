// -*- c++ -*-
/**
 * @file    ClusterTreeMaker.h
 * @author  Joakim Olsson <joakim.olsson@cern.ch>
 * @author  Peter Loch <Peter.Loch@cern.ch>
 * @brief   Athena package to save cell images of clusters for ML training 
 * @date    October 2023
 */

#ifndef MLTREE_CELLTREEMAKER_H
#define MLTREE_CELLTREEMAKER_H

#include <string>
#include <map>
#include <vector>
#include <tuple>

#include <limits>

#include <fstream>

#include <type_traits>

#include <TTree.h>

#include <GaudiKernel/SystemOfUnits.h>

#include "AthenaBaseComps/AthHistogramAlgorithm.h"
// #include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"

#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "CaloConditions/CaloNoise.h"
 
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "CaloEvent/CaloCell.h"
#include "CaloEvent/CaloCellContainer.h"

#include "CaloSimEvent/CaloCalibrationHitContainer.h"

#include "CaloDetDescr/CaloDetDescrManager.h"
#include "CaloDetDescr/CaloDetDescrElement.h"

// #include "CaloCalibHitRec/CalibHitToCaloCellTool.h"

#include "xAODTracking/VertexContainer.h"

///@brief Algorithm to produce flat @c ROOT tuple for ML studies
///
/// This algorithm stores all cell and topo-cluster kinematics at the available scales for fully simulated single particles. 
/// The topo-clusters are provided with the all reconstructed moments as well as relevant moments extracted from calibration 
/// hits. 
///
/// The algorithm is configured using various settings concerning the input and output modes, in addition object filters.  
class CellTreeMaker: public ::AthHistogramAlgorithm { 

public: 
  ///@brief Standard @c AthHistogramAlgorithm algorithm constructor
  CellTreeMaker( const std::string& name, ISvcLocator* pSvcLocator );
  ///@brief Default destructor
  virtual ~CellTreeMaker(); 

  ///@name Implementation of the @c AthHistogramAlgorithm interfaces
  ///@{
  virtual StatusCode  initialize(); ///< Initialization: setup
  virtual StatusCode  execute();    ///< Execution: filling the @c ROOT tuple and writing it
  virtual StatusCode  finalize();   ///< Finalization: summary
  ///@}

private: 

  ///@name Useful type
  ///@{
  ///
  /// Container type linking the @c xAOD::CaloCluster::MomentType with a human-readable moment type, a pointer to the storage location for the moment value 
  /// (the memory address registered with @c ROOT ), and a scale factor for the given moment - typically a measure for the unit.   
  typedef std::tuple<xAOD::CaloCluster::MomentType,std::string,float*,double>                                     MomentLink; //mlink_obj_t;
  /// List/vector of links
  typedef std::vector<MomentLink>                                                                                 MomentLinkVector; //mlink_cnt_t;
  /// Ranked list of paired pointers to clusters, sorted descending in cluster energy
  typedef std::multimap<float,std::tuple<const xAOD::CaloCluster*,const xAOD::CaloCluster*>,std::greater<float> > RankedClusterMap; // rankmap_t;
  /// A container for pointers to non-modifiable @c xAOD::CaloCluster typed objects 
  typedef std::vector<const xAOD::CaloCluster*>                                                                   ClusterVector; //cvect_t;
  /// Cell index range 
  typedef std::tuple<Long64_t,Long64_t> CellIndices; 
  ///@}

  bool               m_debugStreamFlag  = { false           };
  std::string        m_debugStreamName  = { ""              };
  std::ofstream      m_debugStream      = { std::ofstream() };     

  void debugMessage   (const std::string& module,const std::string& fmtStr,...); 
  void resourceMessage(const std::string& module,const std::string& tagStr    );

  ////////////////
  // Properties //
  ////////////////

  // -- input (read) handles
  SG::ReadHandleKey<CaloCellContainer>            m_cellContainerKey            = { "AllCalo"                   };
  SG::ReadHandleKey<xAOD::CaloClusterContainer>   m_clusterContainerKey         = { "CaloCalTopoClusters"       };  
  SG::ReadHandleKey<xAOD::TruthParticleContainer> m_truthParticleContainerKey   = { "TruthParticles"            };
  SG::ReadHandleKey<xAOD::EventInfo>              m_eventInfoKey                = { "EventInfo"                 }; 
  SG::ReadHandleKey<xAOD::VertexContainer>        m_vertexContainerKey          = { "PrimaryVertices"           };
  // SG::ReadHandleKey<CaloCellContainer>            m_cellTruthContainerKey       = { "TruthCellsEtot"            };
  SG::ReadHandleKey<CaloCalibrationHitContainer>  m_activeCalibHitLArKey        = { "LArCalibrationHitActive"   };
  SG::ReadHandleKey<CaloCalibrationHitContainer>  m_inactiveCalibHitLArKey      = { "LArCalibrationHitInactive" };
  SG::ReadHandleKey<CaloCalibrationHitContainer>  m_activeCalibHitTileKey       = { "TileCalibHitActiveCell"    };
  SG::ReadHandleKey<CaloCalibrationHitContainer>  m_inactiveCalibHitTileKey     = { "TileCalibHitInactiveCell"  };

  SG::ReadCondHandleKey<CaloNoise> m_noiseCDOKey{this,"CaloNoiseKey","totalNoise","SG Key of CaloNoise data object"};

  const CaloNoise* m_noise { nullptr };
  //const CaloNoise* m_noise = *noiseHdl;
  const CaloDetDescrManager* m_caloDDM     = { nullptr }; 
  const CaloCell_ID*         m_caloCell_ID = { nullptr };


  // -- container keys and such
  std::string m_prefix                   = { ""                    };

  // -- topo-cluster selection
  double m_clusterEmin                   = { 0.                      };
  double m_clusterEmax                   = { 2000.*Gaudi::Units::GeV };
  double m_clusterRapMin                 = { -5.0   };
  double m_clusterRapMax                 = {  5.0   };

  // -- topo-cluster truth selection
  double m_clusterTruthEmin              = { 0.                      };
  double m_clusterTruthEmax              = { 2000.*Gaudi::Units::GeV };

  // -- cell truth collection 
  double m_cellTruthEmin                 = { 0.                      };
  double m_cellTruthEmax                 = { 2000.*Gaudi::Units::GeV };

  // -- configuration
  bool   m_useLCWScaleAsReference        = { false  };
  bool   m_doUnCalibratedClusters        = { false  };
  bool   m_useAllCells                   = { false  }; // all cells if true, ignore m_useClusteredCells and m_useTruthCells
  bool   m_useClusteredCells             = { true   }; // all clustered cells, ignores m_useTruthCells
  bool   m_useTruthCells                 = { true   }; // all truth cells (cells with true deposited energy), can be combined with m_useClusteredCells
  bool   m_addCellCartesian              = { true   }; // add cartesian coordinates for cells

  // -- moments to be included into the tuple
  std::vector<std::string> m_caloClusterMoments;

  // -- truth tools
  //ToolHandle<CalibHitToCaloCellTool> m_calibHitToCaloCellTool{ this, "CellCalibHitProvider", "", "Handle to the Calib Hit to Calo cell Tool" };
  typedef std::tuple<double,double,double> celldeposit_t;
  typedef std::vector<celldeposit_t>       celldeposit_container_t;
  celldeposit_container_t m_cellTruthEnergies; 

  ////////////////////////////
  // Internal stores/caches //
  ////////////////////////////

  // -- pointer cell container
  const CaloCellContainer* m_recoCellContPtr  = { nullptr }; 
  // const CaloCellContainer* m_truthCellContPtr = { nullptr }; 

  // -- truth particle related
  int m_barCode = { 10001 };

  // -- cache links requested moment with address in tuple
  MomentLinkVector m_caloClusterMomentLink;

  // -- internal event/cluster counters
  int m_allEvntCtr = { 0 };
  int m_accEvntCtr = { 0 };
  int m_allClusCtr = { 0 };
  int m_accClusCtr = { 0 };

  /////////////////////
  // Tuple structure //
  /////////////////////  

  // -- trees
  TTree* m_cellTree    = { nullptr };
  TTree* m_clusterTree = { nullptr };


  // -- event info
  int       m_runNumber   = { 0  };
  long long m_eventNumber = { 0  };
  long long m_sequence    = { 0  };
  float     m_averageMu   = { 0. };
  int       m_nPrimVtx    = { 0  };

  ///@name Particle truth 
  ///@{
  long long m_fParticleCount   = { 0  };
  float     m_fClusterTruthE   = { 0. };
  float     m_fClusterTruthPt  = { 0. };
  float     m_fClusterTruthEta = { 0. };
  float     m_fClusterTruthPhi = { 0. };
  int       m_fClusterTruthPDG = { 0  };
  ///@}

  ///@name Cell data
  ///@{
  float     m_cellSignificance      = { 0. };
  float     m_cellEdep              = { 0. };     // deposited energy in cell
  float     m_cellEem               = { 0. };     // EM scale energy in cell
  float     m_cellEta               = { 0. };     // cell rapidity
  float     m_cellPhi               = { 0. };     // cell azimuth
  float     m_cellTime              = { 0. };     // cell time
  float     m_cellCenterX           = { 0. };     // cell center x-coordinate
  float     m_cellCenterY           = { 0. };     // cell center y-coordinate
  float     m_cellCenterZ           = { 0. };     // cell center z-coordinate
  uint16_t  m_cellQuality           = { 0  };     // cell quality
  uint16_t  m_cellSampling          = { 0  };     // cell sampling
  unsigned int m_cellID             = { 0  };     // cell identifier hash
  long long m_cellClusterLink       = { -1 };     // link to first  particle/cluster entry in ClusterTree
  bool      m_cellInCluster         = { false };  // indicates that cell belong to cluster
  ///@}
  bool m_cellIsTile                 = { false };     // cell geomatry infomation
  bool m_cellIsLAr                  = { false };     // cell geomatry infomation
  bool m_cellIsLArFwd               = { false };     // cell geomatry infomation
  bool m_cellIsLArHEC               = { false };     // cell geomatry infomation

  // -- cluster specs
  int      m_nCluster            = { 0 };
  int      m_fCluster_nCells     = { 0 }; // # cells with E > 0
  int      m_fCluster_nCells_tot = { 0 }; // # all cells
  int      m_fClusterIndex       = { 0 };
  Long64_t m_fCluster_firstCell  = { -1 };
  Long64_t m_fCluster_lastCell   = { -1 }; 

  // -- cluster signals: E0 scale (optional use)
  float m_fClusterE          = { 0. };
  float m_fClusterPt         = { 0. };
  float m_fClusterEta        = { 0. };
  float m_fClusterPhi        = { 0. };
  float m_fCluster_sumCellE  = { 0. };
  float m_fClusterTime       = { 0. };
  float m_fCluster_fracE     = { 0. }; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  float m_fCluster_fracE_ref = { 0. }; // fractional contribution particle (cluster_energy/reference energy)   
  
  // -- cluster signals: LCW scale (default/always use)
  float m_fClusterECalib          = { 0. };
  float m_fClusterPtCalib         = { 0. };
  float m_fClusterEtaCalib        = { 0. };
  float m_fClusterPhiCalib        = { 0. };
  float m_fCluster_sumCellECalib  = { 0. };
  float m_fCluster_fracECalib     = { 0. }; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  float m_fCluster_fracECalib_ref = { 0. }; // fractional contribution particle (cluster_energyreference energy)   

  // -- cluster truth info
  float m_fCluster_ENG_CALIB_TOT       = { 0. };  // energy deposit in cluster cells 
  float m_fCluster_ENG_CALIB_OUT_L     = { 0. };  // energy deposit outside of cluster but associated (loose selection)
  float m_fCluster_ENG_CALIB_OUT_M     = { 0. };  // energy deposit outside of cluster but associated (medium selection) 
  float m_fCluster_ENG_CALIB_OUT_T     = { 0. };  // energy deposit outside of cluster but associated (tight selection)
  float m_fCluster_ENG_CALIB_DEAD_L    = { 0. };  // energy deposit in dead material associated with the cluster (loose selection)  
  float m_fCluster_ENG_CALIB_DEAD_M    = { 0. };  // energy deposit in dead material associated with the cluster (medium selection)
  float m_fCluster_ENG_CALIB_DEAD_T    = { 0. };  // energy deposit in dead material associated with the cluster (tight selection)
  float m_fCluster_ENG_CALIB_DEAD_TOT  = { 0. };  // energy deposit in dead material associated with the cluster (all losses)
  float m_fCluster_ENG_CALIB_FRAC_EM   = { 0. };  // energy deposited by e/gamma inside the cluster
  float m_fCluster_ENG_CALIB_FRAC_HAD  = { 0. };  // energy deposited by charged pions inside the cluster
  float m_fCluster_ENG_CALIB_FRAC_REST = { 0. };  // energy deposited by all other particles in the cluster 

  /// -- calibration weights
  float m_fCluster_EM_PROBABILITY = { 0. };       // EM likelihood
  float m_fCluster_HAD_WEIGHT     = { 0. };       // hadronic weights sum(w_cell x E_cell)/sum(E_cell)
  float m_fCluster_OOC_WEIGHT     = { 0. };       // out-of-cluster correction factor (OOC)
  float m_fCluster_DM_WEIGHT      = { 0. };       // dead material correction factor (DMC)
  float m_fCluster_Ehad           = { 0. };       // hadronic calibration applied:  HAD_WEIGHT * ClusterE
  float m_fCluster_Eooc           = { 0. };       // amount of energy added by OOC: (1-OOC_WEIGHT)*Ehad 
  float m_fCluster_Edmc           = { 0. };       // amount of energy added by DMC: (1-DM_WEIGHT)*(Ehad+Eooc)
 
  // -- cluster moments (used as configured)
  float m_fCluster_CENTER_MAG        = { 0. };    // always used (historic)
  float m_fCluster_FIRST_PHI         = { 0. };
  float m_fCluster_FIRST_ETA         = { 0. };
  float m_fCluster_SECOND_R          = { 0. };
  float m_fCluster_SECOND_LAMBDA     = { 0. };
  float m_fCluster_DELTA_PHI         = { 0. };
  float m_fCluster_DELTA_THETA       = { 0. };
  float m_fCluster_DELTA_ALPHA       = { 0. };
  float m_fCluster_CENTER_X          = { 0. };
  float m_fCluster_CENTER_Y          = { 0. };
  float m_fCluster_CENTER_Z          = { 0. };
  float m_fCluster_CENTER_LAMBDA     = { 0. };
  float m_fCluster_LATERAL           = { 0. };
  float m_fCluster_LONGITUDINAL      = { 0. };
  float m_fCluster_ENG_FRAC_EM       = { 0. };
  float m_fCluster_ENG_FRAC_MAX      = { 0. };
  float m_fCluster_ENG_FRAC_CORE     = { 0. };
  float m_fCluster_FIRST_ENG_DENS    = { 0. };    // always used (historic)
  float m_fCluster_SECOND_ENG_DENS   = { 0. };
  float m_fCluster_ISOLATION         = { 0. };
  float m_fCluster_ENG_BAD_CELLS     = { 0. };
  float m_fCluster_N_BAD_CELLS       = { 0. };
  float m_fCluster_N_BAD_CELLS_CORR  = { 0. };
  float m_fCluster_BAD_CELLS_CORR_E  = { 0. };
  float m_fCluster_BADLARQ_FRAC      = { 0. };
  float m_fCluster_ENG_POS           = { 0. };
  float m_fCluster_SIGNIFICANCE      = { 0. };
  float m_fCluster_CELL_SIGNIFICANCE = { 0. };
  float m_fCluster_CELL_SIG_SAMPLING = { 0. };
  float m_fCluster_AVG_LAR_Q         = { 0. };
  float m_fCluster_AVG_TILE_Q        = { 0. };
  float m_fCluster_ENG_BAD_HV_CELLS  = { 0. };
  float m_fCluster_N_BAD_HV_CELLS    = { 0. };
  float m_fCluster_PTD               = { 0. };
  float m_fCluster_MASS              = { 0. };
  float m_fCluster_SECOND_TIME       = { 0. };

  // -- control and constants
  static std::map<std::string,xAOD::CaloCluster::MomentType> m_knownClusterMomentType;
  static std::map<xAOD::CaloCluster::MomentType,std::string> m_knownClusterMomentName;
  static std::map<xAOD::CaloCluster::MomentType,double>      m_knownClusterMomentScale;

  /////////////
  // Helpers //
  /////////////

  // -- adding cluster moments to the tuple and managing their content
  bool        bookClusterMoments(size_t nwrds=0);             // book 
  void        registerClusterMoment(MomentLink& mlink);       // register moments in lookup table
  void        resetClusterMomentLinkValue(float value=0.);    // reset link values
  bool        resetAll();                                     // reset all variables
  bool        resetClusters();                                // reset cluster variables
  bool        resetCells();                                   // reset cell variables
  // -- select cells and clusters
  bool        applyFilter(const xAOD::CaloCluster& rclus,xAOD::CaloCluster::State s=xAOD::CaloCluster::UNCALIBRATED); // cluster filter
  bool        applyFilter(const CaloCell& cell) const;                                                                // cell filter
  bool        acceptCell(const CaloCell& cell,std::vector<bool>& cellsUsed) const;
  bool        storeCell(const CaloCell& cell, std::vector<bool>& cellsUsed, bool rejectIfUsed =false); 
  bool        storeCell(const CaloCell& cell);
  // -- get deposited energy
  bool        loadDepositedEnergy();
  int         fillDepositedEnergy(const CaloCalibrationHitContainer& ccont,const CaloDetDescrManager* caloDDM,celldeposit_container_t& collectedEnergies); 
  double      depositedEnergy(const CaloCell& cell) const;
  int         caloCellIndex  (const CaloCell& cell) const;

  ///////////////
  // Templates //
  ///////////////

  // -- fill cluster data
  template<class CCONT> bool fillClusters(const CCONT* clusCont,double refEnergy,std::vector<bool>& cellsUsed,xAOD::CaloCluster::State s=xAOD::CaloCluster::UNCALIBRATED) {
    static const std::string mname = "CellTreeMaker::fillClusters(...)";
    // -- topo-cluster counter
    m_allClusCtr += (int)clusCont->size();
    // -- sort and filter
    RankedClusterMap rankedClusters; double eSum(0.); double eSumCalib(0.); 
    for ( size_t i(0); i<clusCont->size(); ++i ) { 
      const xAOD::CaloCluster* calClus = clusCont->at(i);                                                  // allocate cluster
      const xAOD::CaloCluster* rawClus = nullptr; 
      if ( m_doUnCalibratedClusters ) {                                                                    // old style provides geometrical cell weights
	rawClus = calClus->getSisterCluster();
	if ( rawClus == nullptr ) { 
	  ATH_MSG_ERROR("invalid null-pointer to sister cluster for cal cluster " << calClus->index());
	  return false; 
	}
	if ( calClus->index() != rawClus->index() ) { 
	  ATH_MSG_ERROR("index mismatch: raw/cal index " << rawClus->index() << "/" << calClus->index());
	  return false;
	} 
      } else {
	rawClus = calClus; 
      }
      // check scale preference and apply corresponding filter and sorting argument
      if ( applyFilter(*calClus,s) ) {
	eSum += calClus->e(xAOD::CaloCluster::UNCALIBRATED); eSumCalib += calClus->e(xAOD::CaloCluster::CALIBRATED);  // always uncalibrated/calibrated, independent of s 
	rankedClusters.emplace_hint(rankedClusters.end(),calClus->e(s)/Gaudi::Units::GeV,std::make_tuple(calClus,rawClus)); 
      }
    }
    // -- total number of surviving clusters
    m_nCluster = static_cast<int>(rankedClusters.size());
    // -- accepted event and cluster counters
    ++m_accEvntCtr; m_accClusCtr += m_nCluster;
    m_fClusterIndex = 0;
    if ( m_nCluster == 0 ) { 
      ++m_sequence; 
      debugMessage(mname,"# ----- [clusters] - checking: found %i ranked clusters only (failed)",m_nCluster); 
      return false; 
    }
    // -- loop over clusters in order of their energies
    bool isOk(true); 

    size_t ncluscells(0);
    for(const auto& mpair : rankedClusters ) {
      // reset all previously filled data
      resetClusters(); debugMessage(mname,"# ----- [clusters] - reset all cluster variables %i/%i",m_fClusterIndex+1,m_nCluster);
      // allocate clusters
      const xAOD::CaloCluster* calClus = std::get<0>(mpair.second);
      const xAOD::CaloCluster* rawClus = std::get<1>(mpair.second);
      if ( rawClus == nullptr ) { ATH_MSG_ERROR( "unexpected invalid pointer to raw signal cluster raw = " << rawClus << " cal = " << calClus ); isOk = false; break; }
      // basic scale clusters
      m_fClusterE          = rawClus->e  (xAOD::CaloCluster::UNCALIBRATED) /Gaudi::Units::GeV   ;
      m_fClusterPt         = rawClus->pt (xAOD::CaloCluster::UNCALIBRATED) /Gaudi::Units::GeV   ;
      m_fClusterEta        = rawClus->eta(xAOD::CaloCluster::UNCALIBRATED)                      ;
      m_fClusterPhi        = rawClus->phi(xAOD::CaloCluster::UNCALIBRATED) /Gaudi::Units::radian;
      m_fClusterTime       = rawClus->time()/Gaudi::Units::nanosecond;
      m_fCluster_fracE     = eSum != 0.      ? m_fClusterE/eSum      : 0.;
      m_fCluster_fracE_ref = refEnergy != 0. ? m_fClusterE/refEnergy : 0.;
      // LCW scale clusters
      m_fClusterECalib          = calClus->e  (xAOD::CaloCluster::CALIBRATED)/Gaudi::Units::GeV   ; 
      m_fClusterPtCalib         = calClus->pt (xAOD::CaloCluster::CALIBRATED)/Gaudi::Units::GeV   ;
      m_fClusterEtaCalib        = calClus->eta(xAOD::CaloCluster::CALIBRATED)                     ;
      m_fClusterPhiCalib        = calClus->phi(xAOD::CaloCluster::CALIBRATED)/Gaudi::Units::radian;
      m_fCluster_fracECalib     = eSumCalib != 0. ? m_fClusterECalib/eSumCalib : 0.;
      m_fCluster_fracECalib_ref = refEnergy != 0. ? m_fClusterECalib/refEnergy : 0.;
      // -- collect truth moments
      double mval(0.);
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_TOT,      mval) ) { m_fCluster_ENG_CALIB_TOT       = -1.; } else { m_fCluster_ENG_CALIB_TOT       = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_TOT      ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_T,    mval) ) { m_fCluster_ENG_CALIB_OUT_T     = -1.; } else { m_fCluster_ENG_CALIB_OUT_T     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_OUT_T    ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_L,    mval) ) { m_fCluster_ENG_CALIB_OUT_L     = -1.; } else { m_fCluster_ENG_CALIB_OUT_L     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_OUT_L    ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_OUT_M,    mval) ) { m_fCluster_ENG_CALIB_OUT_M     = -1.; } else { m_fCluster_ENG_CALIB_OUT_M     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_OUT_M    ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_TOT, mval) ) { m_fCluster_ENG_CALIB_DEAD_TOT  = -1.; } else { m_fCluster_ENG_CALIB_DEAD_TOT  = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_TOT ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_T,   mval) ) { m_fCluster_ENG_CALIB_DEAD_T    = -1.; } else { m_fCluster_ENG_CALIB_DEAD_T    = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_T   ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_L,   mval) ) { m_fCluster_ENG_CALIB_DEAD_L    = -1.; } else { m_fCluster_ENG_CALIB_DEAD_L    = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_L   ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_DEAD_M,   mval) ) { m_fCluster_ENG_CALIB_DEAD_M    = -1.; } else { m_fCluster_ENG_CALIB_DEAD_M    = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_DEAD_M   ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_FRAC_EM,  mval) ) { m_fCluster_ENG_CALIB_FRAC_EM   = -1.; } else { m_fCluster_ENG_CALIB_FRAC_EM   = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_FRAC_EM  ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_FRAC_HAD, mval) ) { m_fCluster_ENG_CALIB_FRAC_HAD  = -1.; } else { m_fCluster_ENG_CALIB_FRAC_HAD  = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_FRAC_HAD ); }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::ENG_CALIB_FRAC_REST,mval) ) { m_fCluster_ENG_CALIB_FRAC_REST = -1.; } else { m_fCluster_ENG_CALIB_FRAC_REST = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_FRAC_REST); }
      // -- collect calibration factors and EM likelihood
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::HAD_WEIGHT,    mval) ) { m_fCluster_HAD_WEIGHT     = -1.; } else { m_fCluster_HAD_WEIGHT     = mval; } 
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::OOC_WEIGHT,    mval) ) { m_fCluster_OOC_WEIGHT     = -1.; } else { m_fCluster_OOC_WEIGHT     = mval; }
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::DM_WEIGHT,     mval) ) { m_fCluster_DM_WEIGHT      = -1.; } else { m_fCluster_DM_WEIGHT      = mval; }
      if ( !calClus->retrieveMoment( xAOD::CaloCluster::EM_PROBABILITY,mval) ) { m_fCluster_EM_PROBABILITY = -1.; } else { m_fCluster_EM_PROBABILITY = mval; }
      // -- retrieve static moments (deprecated)
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::CENTER_MAG,    mval) ) { m_fCluster_CENTER_MAG     = -1.; } else { m_fCluster_CENTER_MAG     = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::CENTER_MAG);     }
      if ( !rawClus->retrieveMoment( xAOD::CaloCluster::FIRST_ENG_DENS,mval) ) { m_fCluster_FIRST_ENG_DENS = -1.; } else { m_fCluster_FIRST_ENG_DENS = mval * m_knownClusterMomentScale.at(xAOD::CaloCluster::FIRST_ENG_DENS); } 
      // -- dynamically scheduled reconstructed moments
      for ( auto& mlnk : m_caloClusterMomentLink ) {
	if ( !rawClus->retrieveMoment(std::get<0>(mlnk),mval) ) { *(std::get<2>(mlnk)) = -1.; } else { *(std::get<2>(mlnk)) = mval * std::get<3>(mlnk); } 
      }
      // -- cluster composition and nomimal sums
      m_fCluster_nCells        = 0;
      m_fCluster_nCells_tot    = 0;
      m_fCluster_sumCellE      = 0.;
      m_fCluster_sumCellECalib = 0.;
      // Note: if rawClus is sister cluster, cell energy weights do not include LCW!
      for ( auto fcell(rawClus->cell_begin()); fcell != rawClus->cell_end(); ++fcell ) { if ( (*fcell)->e() > 0. ) { m_fCluster_sumCellE += (*fcell)->e()*fcell.weight(); ++m_fCluster_nCells; } ++m_fCluster_nCells_tot; }
      if ( rawClus == calClus ) {
	m_fCluster_sumCellE      /= Gaudi::Units::GeV; 
	m_fCluster_sumCellECalib  = m_fCluster_sumCellE;
	if ( m_fCluster_sumCellECalib != 0. ) { m_fCluster_sumCellE *= (m_fClusterE/m_fClusterECalib); }  // cell weights include LCW -> unddo total effect of LCW on topo-cluster from EM scale sum
      } else {
	for ( auto fcell(calClus->cell_begin()); fcell != calClus->cell_end(); ++fcell ) { if ( (*fcell)->e() > 0. ) { m_fCluster_sumCellECalib += (*fcell)->e()/Gaudi::Units::GeV*fcell.weight();                     }                         }
      }
      // fill cluster tuple structure
      ++m_sequence; 

      // fill cells from cluster
      if ( m_useClusteredCells ) { 
	m_cellClusterLink  = m_clusterTree->GetEntries(); 
	std::vector<const CaloCell*> cellsInCluster;
	for ( auto fcell(rawClus->cell_begin()); fcell != rawClus->cell_end(); ++fcell ) { cellsInCluster.push_back(*fcell); }
	ncluscells += cellsInCluster.size();
	// ATH_MSG_INFO( "collected " << cellsInCluster.size() << " cells from cluster" );
	m_cellInCluster   = true; 
	m_cellClusterLink = m_clusterTree->GetEntries();
	auto cellIndices = fillCells<decltype(cellsInCluster)>(&cellsInCluster,cellsUsed,true,false);
	if ( std::get<0>(cellIndices) <= std::get<1>(cellIndices) ) { m_fCluster_firstCell = std::get<0>(cellIndices); m_fCluster_lastCell = std::get<1>(cellIndices); } 
      }  
      m_clusterTree->Fill();
      m_fCluster_firstCell = -1;
      m_fCluster_lastCell  = -1;
      ++m_fClusterIndex;
    } // loop on ranked clusters
    if ( !isOk ) { 
      debugMessage(mname,"# ----- [clusters] - checking: found invalid pointer to sister cluster (failed)"); 
    }
    // ATH_MSG_INFO( "filled " << ncluscells << " cells from clusters" );
    return isOk;
  }

  // fill cells with tracking and truth filter
  template<class CCONT> CellIndices fillCellsWithTruth(const CCONT* cellCont,std::vector<bool>& cellsUsed,bool rejectIfUsed=false) {
    // loop cells
    int ncells(0); 
    CellIndices cellIdx{ m_cellTree->GetEntries(),-1 };
    for ( const auto cptr : *cellCont ) { if ( applyFilter(*cptr) ) { storeCell(*cptr,cellsUsed,rejectIfUsed); ++ncells; } }
    if ( ncells > 0 ) { --ncells; std::get<1>(cellIdx) = std::get<0>(cellIdx) + ncells; }
    return cellIdx;
  }

  // fill cells with tracking and no filter
  template<class CCONT> CellIndices fillCellsNoFilter(const CCONT* cellCont,std::vector<bool>& cellsUsed,bool rejectIfUsed=false) {
    // loop cells
    int ncells(0); 
    CellIndices cellIdx{ m_cellTree->GetEntries(), -1 };
    for ( const auto cptr : *cellCont ) { storeCell(*cptr,cellsUsed,rejectIfUsed); ++ncells; }
    if ( ncells > 0 ) { --ncells; std::get<1>(cellIdx) = std::get<0>(cellIdx) + ncells; }
    return cellIdx;
  }

  // fill cell general interface
  template<class CCONT> CellIndices fillCells(const CCONT* cellCont,std::vector<bool>& cellsUsed,bool allCells=true,bool rejectIfUsed=false) {
    // loop cells
    return allCells ? fillCellsNoFilter<CCONT>(cellCont,cellsUsed,rejectIfUsed) : fillCellsWithTruth<CCONT>(cellCont,cellsUsed,rejectIfUsed); 
  }

  // fill cell data without tracking
  template<class CCONT> CellIndices fillCells(const CCONT* cellCont) {
    // loop cells
    int ncells(0);
    CellIndices cellIdx{ m_cellTree->GetEntries(), -1 }; 
    for ( const auto cptr : *cellCont ) { if ( applyFilter(*cptr) ) { storeCell(*cptr); ++ncells; } }
    if ( ncells > 0 ) { --ncells; std::get<1>(cellIdx) = std::get<0>(cellIdx) + ncells; }
    return cellIdx;
  }
}; 

inline int    CellTreeMaker::caloCellIndex  (const CaloCell& cell) const { return m_recoCellContPtr != nullptr ? m_recoCellContPtr->findIndex(cell.caloDDE()->calo_hash()) : -1; }
inline bool   CellTreeMaker::applyFilter    (const CaloCell& cell) const { return depositedEnergy(cell) > m_cellTruthEmin && depositedEnergy(cell) < m_cellTruthEmax; }
inline double CellTreeMaker::depositedEnergy(const CaloCell& cell) const { 
  if ( m_recoCellContPtr==nullptr || m_recoCellContPtr->size()!=m_cellTruthEnergies.size() ) { return 0.; }
  return caloCellIndex(cell) >= 0 ? std::get<0>(m_cellTruthEnergies.at(caloCellIndex(cell))) : 0.;
}

#endif
