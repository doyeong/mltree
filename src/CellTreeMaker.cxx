#include "MLTree/CellTreeMaker.h"

// Gaudi stuff
#include <GaudiKernel/MsgStream.h>

// StoreGate stuff
#include "StoreGate/ReadHandle.h"

// Utilities
#include "FourMomUtils/xAODP4Helpers.h"
#include "MLTree/SystemResources.h"

// STL
#include <string>
#include <vector>
#include <map>         // for multimap
#include <algorithm>
#include <memory>
#include <tuple>

// C headers
#include <cmath>
#include <cstdio>
#include <cstdarg>

// C I/O
#include <iostream>

#include <algorithm>

#include <type_traits>

#define  TYPE_ENTRY( NAME ) { #NAME , xAOD::CaloCluster:: NAME }
#define  NAME_ENTRY( NAME ) { xAOD::CaloCluster:: NAME , #NAME }

#ifndef CTM_MNAME_W
#define CTM_MNAME_W 32
#endif

namespace {
  static const std::map<bool,std::string> boolToName                      = { { true, "true"                      }, { false, "false"                      } };
  static const std::map<bool,const char*> boolToChar                      = { { true, boolToName.at(true).c_str() }, { false, boolToName.at(false).c_str() } };
  static const std::map<std::string,bool> nameToBool                      = { { "true", true                      }, { "false", false                      } };
  static const std::map<bool,std::string> boolToScale                     = { { true, "LCW"                       }, { false, "EM"                         } };
  static const std::map<std::string,bool> scaleToBool                     = { { "LCW", true                       }, { "EM", false                         } };
  static const std::map<xAOD::CaloCluster::State,std::string> enumToScale = { { xAOD::CaloCluster::UNCALIBRATED, "EM" }, { xAOD::CaloCluster::CALIBRATED, "LCW" }, { xAOD::CaloCluster::ALTCALIBRATED, "ALT" }, { xAOD::CaloCluster::UNKNOWN, "UNKNONW" } };
  static const std::map<std::string,xAOD::CaloCluster::State> scaleToEnum = { { "EM", xAOD::CaloCluster::UNCALIBRATED }, { "LCW", xAOD::CaloCluster::CALIBRATED }, {"ALT",  xAOD::CaloCluster::ALTCALIBRATED }, { "UNKNOWN", xAOD::CaloCluster::UNKNOWN } };
  static const std::map<StatusCode,std::string>               codeToName  = { { StatusCode::SUCCESS, "success" }, { StatusCode::FAILURE, "failed_" }, { StatusCode::RECOVERABLE, "recover" } };
}   

//////////////////
// Dictionaries //
//////////////////

// -- name-to-enum: std::string -> xAOD::CaloCluster::MomentType
std::map<std::string,xAOD::CaloCluster::MomentType> CellTreeMaker::m_knownClusterMomentType = { 
  TYPE_ENTRY( CENTER_MAG        ),
  TYPE_ENTRY( FIRST_PHI         ),
  TYPE_ENTRY( FIRST_ETA         ),
  TYPE_ENTRY( SECOND_R          ),
  TYPE_ENTRY( SECOND_LAMBDA     ),
  TYPE_ENTRY( DELTA_PHI         ),
  TYPE_ENTRY( DELTA_THETA       ),
  TYPE_ENTRY( DELTA_ALPHA       ),
  TYPE_ENTRY( CENTER_X          ),
  TYPE_ENTRY( CENTER_Y          ),
  TYPE_ENTRY( CENTER_Z          ),
  TYPE_ENTRY( CENTER_LAMBDA     ),
  TYPE_ENTRY( LATERAL           ),
  TYPE_ENTRY( LONGITUDINAL      ),
  TYPE_ENTRY( ENG_FRAC_EM       ),
  TYPE_ENTRY( ENG_FRAC_MAX      ),
  TYPE_ENTRY( ENG_FRAC_CORE     ),
  TYPE_ENTRY( FIRST_ENG_DENS    ),
  TYPE_ENTRY( SECOND_ENG_DENS   ),
  TYPE_ENTRY( ISOLATION         ),
  TYPE_ENTRY( ENG_BAD_CELLS     ),
  TYPE_ENTRY( N_BAD_CELLS       ),
  TYPE_ENTRY( N_BAD_CELLS_CORR  ),
  TYPE_ENTRY( BAD_CELLS_CORR_E  ),
  TYPE_ENTRY( BADLARQ_FRAC      ),
  TYPE_ENTRY( ENG_POS           ),
  TYPE_ENTRY( SIGNIFICANCE      ),
  TYPE_ENTRY( CELL_SIGNIFICANCE ),
  TYPE_ENTRY( CELL_SIG_SAMPLING ),
  TYPE_ENTRY( AVG_LAR_Q         ),
  TYPE_ENTRY( AVG_TILE_Q        ),
  TYPE_ENTRY( ENG_BAD_HV_CELLS  ),
  TYPE_ENTRY( N_BAD_HV_CELLS    ),
  TYPE_ENTRY( PTD               ),
  TYPE_ENTRY( MASS              ),
  TYPE_ENTRY( EM_PROBABILITY    ),
  TYPE_ENTRY( SECOND_TIME       )
};

// -- enum to name: xAOD::CaloCluster::MomentType -> std::string 
std::map<xAOD::CaloCluster::MomentType,std::string> CellTreeMaker::m_knownClusterMomentName = { 
  NAME_ENTRY( CENTER_MAG        ),
  NAME_ENTRY( FIRST_PHI         ),
  NAME_ENTRY( FIRST_ETA         ),
  NAME_ENTRY( SECOND_R          ),
  NAME_ENTRY( SECOND_LAMBDA     ),
  NAME_ENTRY( DELTA_PHI         ),
  NAME_ENTRY( DELTA_THETA       ),
  NAME_ENTRY( DELTA_ALPHA       ),
  NAME_ENTRY( CENTER_X          ),
  NAME_ENTRY( CENTER_Y          ),
  NAME_ENTRY( CENTER_Z          ),
  NAME_ENTRY( CENTER_LAMBDA     ),
  NAME_ENTRY( LATERAL           ),
  NAME_ENTRY( LONGITUDINAL      ),
  NAME_ENTRY( ENG_FRAC_EM       ),
  NAME_ENTRY( ENG_FRAC_MAX      ),
  NAME_ENTRY( ENG_FRAC_CORE     ),
  NAME_ENTRY( FIRST_ENG_DENS    ),
  NAME_ENTRY( SECOND_ENG_DENS   ),
  NAME_ENTRY( ISOLATION         ),
  NAME_ENTRY( ENG_BAD_CELLS     ),
  NAME_ENTRY( N_BAD_CELLS       ),
  NAME_ENTRY( N_BAD_CELLS_CORR  ),
  NAME_ENTRY( BAD_CELLS_CORR_E  ),
  NAME_ENTRY( BADLARQ_FRAC      ),
  NAME_ENTRY( ENG_POS           ),
  NAME_ENTRY( SIGNIFICANCE      ),
  NAME_ENTRY( CELL_SIGNIFICANCE ),
  NAME_ENTRY( CELL_SIG_SAMPLING ),
  NAME_ENTRY( AVG_LAR_Q         ),
  NAME_ENTRY( AVG_TILE_Q        ),
  NAME_ENTRY( ENG_BAD_HV_CELLS  ),
  NAME_ENTRY( N_BAD_HV_CELLS    ),
  NAME_ENTRY( PTD               ),
  NAME_ENTRY( MASS              ),
  NAME_ENTRY( EM_PROBABILITY    ),
  NAME_ENTRY( SECOND_TIME       )
};

// -- scale assignment
std::map<xAOD::CaloCluster::MomentType,double> CellTreeMaker::m_knownClusterMomentScale = { 
  { xAOD::CaloCluster::CENTER_MAG         , 1.0                          },
  { xAOD::CaloCluster::FIRST_PHI          , 1.0                          },
  { xAOD::CaloCluster::FIRST_ETA          , 1.0                          },
  { xAOD::CaloCluster::SECOND_R           , 1./Gaudi::Units::millimeter2 },
  { xAOD::CaloCluster::SECOND_LAMBDA      , 1./Gaudi::Units::millimeter2 },
  { xAOD::CaloCluster::DELTA_PHI          , 1./Gaudi::Units::radian      },
  { xAOD::CaloCluster::DELTA_THETA        , 1./Gaudi::Units::radian      },
  { xAOD::CaloCluster::DELTA_ALPHA        , 1./Gaudi::Units::radian      },
  { xAOD::CaloCluster::CENTER_X           , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::CENTER_Y           , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::CENTER_Z           , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::CENTER_LAMBDA      , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::LATERAL            , 1.0                          },
  { xAOD::CaloCluster::LONGITUDINAL       , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_EM        , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_MAX       , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_CORE      , 1.0                          },
  { xAOD::CaloCluster::FIRST_ENG_DENS     , 1./(Gaudi::Units::GeV/Gaudi::Units::millimeter3)                                                 },
  { xAOD::CaloCluster::SECOND_ENG_DENS    , 1./((Gaudi::Units::GeV/Gaudi::Units::millimeter3)*(Gaudi::Units::GeV/Gaudi::Units::millimeter3)) },
  { xAOD::CaloCluster::ISOLATION          , 1.0                          },
  { xAOD::CaloCluster::ENG_BAD_CELLS      , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::N_BAD_CELLS        , 1.0                          },
  { xAOD::CaloCluster::N_BAD_CELLS_CORR   , 1.0                          },
  { xAOD::CaloCluster::BAD_CELLS_CORR_E   , 1.0                          },
  { xAOD::CaloCluster::BADLARQ_FRAC       , 1.0                          },
  { xAOD::CaloCluster::ENG_POS            , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::SIGNIFICANCE       , 1.0                          },
  { xAOD::CaloCluster::CELL_SIGNIFICANCE  , 1.0                          },
  { xAOD::CaloCluster::CELL_SIG_SAMPLING  , 1.0                          },
  { xAOD::CaloCluster::AVG_LAR_Q          , 1.0                          },
  { xAOD::CaloCluster::AVG_TILE_Q         , 1.0                          },
  { xAOD::CaloCluster::ENG_BAD_HV_CELLS   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::N_BAD_HV_CELLS     , 1.0                          },
  { xAOD::CaloCluster::PTD                , 1.0                          },
  { xAOD::CaloCluster::MASS               , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::EM_PROBABILITY     , 1.0                          },
  { xAOD::CaloCluster::ENG_CALIB_TOT      , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_OUT_T    , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_OUT_L    , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_OUT_M    , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_T   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_L   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_M   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_TOT , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_FRAC_EM  , 1.0                          },
  { xAOD::CaloCluster::ENG_CALIB_FRAC_HAD , 1.0                          },
  { xAOD::CaloCluster::ENG_CALIB_FRAC_REST, 1.0                          },
  { xAOD::CaloCluster::SECOND_TIME        , 1./(Gaudi::Units::nanosecond*Gaudi::Units::nanosecond) }
};

namespace {
  // MsgStream& operator<<(MsgStream& mstr,const SG::ReadHandleKey<xAOD::TruthParticleContainer>& ckey) { mstr << ckey.key(); return mstr; }
  // MsgStream& operator<<(MsgStream& mstr,const SG::ReadHandleKey<xAOD::CaloClusterContainer>&   ckey) { mstr << ckey.key(); return mstr; }
  // MsgStream& operator<<(MsgStream& mstr,const SG::ReadHandleKey<xAOD::EventInfo>&              ckey) { mstr << ckey.key(); return mstr; }
  template<class HANDLEKEY> std::string handleKey(const HANDLEKEY& hkey ) { return hkey.key(); }
}

/////////////////
// Constructor //
/////////////////

CellTreeMaker::CellTreeMaker( const std::string& name, ISvcLocator* pSvcLocator ) 
  : AthHistogramAlgorithm( name, pSvcLocator )
{
  // -- data 
  declareProperty("Prefix",                m_prefix                   );
  declareProperty("CaloCellContainer"     ,m_cellContainerKey         );
  declareProperty("CaloClusterContainer",  m_clusterContainerKey      );
  declareProperty("TruthParticleContainer",m_truthParticleContainerKey);  
  declareProperty("EventInfo",             m_eventInfoKey             );
					   
  // -- topo-cluster phasespace			   
  declareProperty("ClusterEmin",           m_clusterEmin             ,"minimum topo-cluster signal at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale"   );
  declareProperty("ClusterEmax",           m_clusterEmax             ,"maximum topo-cluster signal at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale"   );
  declareProperty("ClusterRapMin",         m_clusterRapMin           ,"minimum topo-cluster rapidity at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale ");
  declareProperty("ClusterRapMax",         m_clusterRapMax           ,"maximum topo-cluster rapidity at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale ");

  // -- topo-cluster truth phase space
  declareProperty("ClusterTruthEmin",      m_clusterTruthEmin        ,"minimum true (deposited) energy in topo-cluster");
  declareProperty("ClusterTruthEmax",      m_clusterTruthEmax        ,"maximum true (deposited) energy in topo-cluster");

  // -- cell phasespace
  declareProperty("CellTruthEmin",         m_cellTruthEmin           ,"minimum true (deposited) energy in cell"        );
  declareProperty("CellTruthEmax",         m_cellTruthEmax           ,"maximum true (deposited) energy in cell"        );

  // -- tuple configuration
  declareProperty("CaloClusterMomentNames",m_caloClusterMoments      ,"list of topo-cluster moment names to be written out" );

  // -- process control
  declareProperty("UncalibratedClusters"  ,m_doUnCalibratedClusters  ,"include uncalibrated (EM) scale clusters"                                             );
  declareProperty("UseLCWScaleAsReference",m_useLCWScaleAsReference  ,"use LCW kinematics for cluster selection, instead of EM"                              );
  declareProperty("UncalibratedClusters"  ,m_doUnCalibratedClusters  ,"include uncalibrated (EM) scale clusters"                                             );
  declareProperty("DebugStreamFlag"       ,m_debugStreamFlag         ,"write additional debugging information to file"                                       );
  declareProperty("DebugStreamFile"       ,m_debugStreamName         ,"name of file associated with debug stream"                                            );
  declareProperty("UseAllCells"           ,m_useAllCells             ,"flag indicating that all cells should be written to tuple"                            ); 
  declareProperty("UseClusteredCells"     ,m_useClusteredCells       ,"flag indicating that all cells in topo-clusters should be written to tuple"           );
  declareProperty("UseTruthCells"         ,m_useTruthCells           ,"flag indicating that all cells with true depostied energy should be written to tuple" );
  declareProperty("AddCellCartesians"     ,m_addCellCartesian        ,"flag for adding catersian coordinates for all cells"                                  );

}

CellTreeMaker::~CellTreeMaker() {}

////////////////////
// Initialization //
////////////////////

StatusCode CellTreeMaker::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  char buffer0[128];

  // -- need a prefix (?)
  if (m_prefix=="") { ATH_MSG_WARNING("No decoration prefix name provided"); }

  // -- configuration check
  if ( m_useAllCells ) { 
    if ( m_useClusteredCells || m_useTruthCells) { 
      sprintf(buffer0,"Request: [A] all cells: %s [B] clustered cells: %s [C] truth cells: %s deposited cell energy > %.3f GeV",
	      boolToChar.at(m_useAllCells),
	      boolToChar.at(m_useClusteredCells),
	      boolToChar.at(m_useTruthCells),
	      m_cellTruthEmin);
      std::string stateStr(buffer0);
      m_useClusteredCells = false; 
      m_useTruthCells     = false; 
      m_cellTruthEmin     = -1.*Gaudi::Units::GeV;
      sprintf(buffer0,"%s | Use [A]: %s [B]: %s [C]: %s deposited cell energy > %.3f GeV",stateStr.c_str(),boolToChar.at(m_useAllCells),boolToChar.at(m_useClusteredCells),boolToChar.at(m_useTruthCells),m_cellTruthEmin);
      ATH_MSG_WARNING( buffer0 );
    }
  }

  // -- report configuration
  sprintf(buffer0,"Property: Prefix .................................... \042%s\042",m_prefix.c_str()                              ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: CaloClusterContainer ...................... \042%s\042",handleKey(m_clusterContainerKey).c_str()      ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: TruthParticleContainer .................... \042%s\042",handleKey(m_truthParticleContainerKey).c_str()); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: CaloCellContainer ......................... \042%s\042",handleKey(m_cellContainerKey).c_str()         ); ATH_MSG_INFO( buffer0 );
  // sprintf(buffer0,"Property: TruthCellContainer ........................ \042%s\042",handleKey(m_cellTruthContainerKey).c_str()    ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: LAr active calibration hit container ...... \042%s\042",handleKey(m_activeCalibHitLArKey   ).c_str()  ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: LAr inactive calibration hit container .... \042%s\042",handleKey(m_inactiveCalibHitLArKey ).c_str()  ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: Tile active calibration hit container ..... \042%s\042",handleKey(m_activeCalibHitTileKey  ).c_str()  ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: Tile inactive calibration hit container ... \042%s\042",handleKey(m_inactiveCalibHitTileKey).c_str()  ); ATH_MSG_INFO( buffer0 );
  // ----- exception only for truth to avoid scale factor application for each cluster!
  // m_clusterTruthEmin *= m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_TOT); 
  // m_clusterTruthEmax *= m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_TOT); 
  sprintf(buffer0,"Property: EventInfo ............................... \042%s\042"      ,handleKey(m_eventInfoKey).c_str()                                       ); ATH_MSG_INFO( buffer0 );  
  sprintf(buffer0,"Property: ClusterEmin ........................[GeV] %.3f"            ,m_clusterEmin/Gaudi::Units::GeV                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterEmax ........................[GeV] %.3f"            ,m_clusterEmax/Gaudi::Units::GeV                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterTruthEmin ...................[GeV] %.3f"            ,m_clusterTruthEmin/Gaudi::Units::GeV                                    ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterTruthEmax ...................[GeV] %.3f"            ,m_clusterTruthEmax/Gaudi::Units::GeV                                    ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterRapMin ........................... %.1f"            ,m_clusterRapMin                                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterRapMax ........................... %.1f"            ,m_clusterRapMax                                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: CaloClusterMomentNames .................... %s - %zu names",boolToChar.at(!m_caloClusterMoments.empty()),m_caloClusterMoments.size()); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseLCWasReference ......................... %s"            ,boolToChar.at(m_useLCWScaleAsReference)                                 ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: Debug stream activated .................... %s"            ,boolToChar.at(m_debugStreamFlag)                                        ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: Debug stream filename ..................... %s"            ,m_debugStreamName.c_str()                                               ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: CellTruthEmin ........................[GeV] %.3f"          ,m_cellTruthEmin/Gaudi::Units::GeV                                       ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: CellTruthEmax ........................[GeV] %.3f"          ,m_cellTruthEmax/Gaudi::Units::GeV                                       ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseAllCells ............................... %s"            ,boolToChar.at(m_useAllCells)                                            ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseClusteredCells ......................... %s"            ,boolToChar.at(m_useClusteredCells)                                      ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseTruthCells ............................. %s"            ,boolToChar.at(m_useTruthCells)                                          ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: Add cartesian cell coordinates ............ %s"            ,boolToChar.at(m_addCellCartesian)                                       ); ATH_MSG_INFO( buffer0 );

  // -- check read handle keys 
  ATH_CHECK(m_cellContainerKey.initialize()         );
  // ATH_CHECK(m_cellTruthContainerKey.initialize()    );
  ATH_CHECK(m_activeCalibHitLArKey.initialize()     );
  ATH_CHECK(m_inactiveCalibHitLArKey.initialize()   );
  ATH_CHECK(m_activeCalibHitTileKey.initialize()    );
  ATH_CHECK(m_inactiveCalibHitTileKey.initialize()  );
  ATH_CHECK(m_clusterContainerKey.initialize()      );
  ATH_CHECK(m_eventInfoKey.initialize()             ); 
  ATH_CHECK(m_truthParticleContainerKey.initialize());
  ATH_CHECK(m_vertexContainerKey.initialize()       );
  // ATH_CHECK(m_caloDetDescrMgrKey.initialize()       );
  ATH_CHECK( m_noiseCDOKey.initialize() );
  ATH_CHECK( detStore()->retrieve(m_caloCell_ID) ); 
  // -- book cluster and cell tree TTree and its branches
  CHECK( book(TTree("ClusterTree","ClusterTree")) ); m_clusterTree = tree("ClusterTree");
  CHECK( book(TTree("CellTree"   ,"CellTree"   )) ); m_cellTree    = tree("CellTree"   );
  if ( m_clusterTree != nullptr ) { ATH_MSG_INFO( "allocated TTree \042" << m_clusterTree->GetName() << "\042" ); } else { ATH_MSG_ERROR( "failed to access TTree \042ClusterTree\042" ); return StatusCode::FAILURE; }
  if ( m_cellTree    != nullptr ) { ATH_MSG_INFO( "allocated TTree \042" << m_cellTree->GetName()    << "\042" ); } else { ATH_MSG_ERROR( "failed to access TTree \042CellTree\042"    ); return StatusCode::FAILURE; }

  // -- event info 
  m_clusterTree->Branch("seqNumber",  &m_sequence,   "seqNumber/L"  );
  m_clusterTree->Branch("runNumber",  &m_runNumber,  "runNumber/I"  );
  m_clusterTree->Branch("eventNumber",&m_eventNumber,"eventNumber/L");
  m_clusterTree->Branch("avgMu"      ,&m_averageMu  ,"avgMu/F"      );
  m_clusterTree->Branch("nPrimVtx"   ,&m_nPrimVtx   ,"nPrimVtx/I"   );

  // -- truth particle kinematics and id
  m_clusterTree->Branch("truthCnt",&m_fParticleCount  ,"truthCnt/I");
  m_clusterTree->Branch("truthE"  ,&m_fClusterTruthE  ,"truthE/F"  );
  m_clusterTree->Branch("truthPt" ,&m_fClusterTruthPt ,"truthPt/F" );
  m_clusterTree->Branch("truthEta",&m_fClusterTruthEta,"truthEta/F");
  m_clusterTree->Branch("truthPhi",&m_fClusterTruthPhi,"truthPhi/F");
  m_clusterTree->Branch("truthPDG",&m_fClusterTruthPDG,"truthPDG/I");

  // -- cluster indices etc.
  m_clusterTree->Branch("nCluster"          ,&m_nCluster           ,"nCluster/I"          );
  m_clusterTree->Branch("clusterIndex"      ,&m_fClusterIndex      ,"clusterIndex/I"      );
  m_clusterTree->Branch("cluster_nCells"    ,&m_fCluster_nCells    ,"cluster_nCells/I"    );
  m_clusterTree->Branch("cluster_nCells_tot",&m_fCluster_nCells_tot,"cluster_nCells_tot/I");
  m_clusterTree->Branch("cluster_firstCell" ,&m_fCluster_firstCell ,"cluster_firstCell/L" );
  m_clusterTree->Branch("cluster_lastCell"  ,&m_fCluster_lastCell  ,"cluster_lastCell/L"  );

  // -- cluster kinematics: LCW scale
  m_clusterTree->Branch("clusterECalib"         ,&m_fClusterECalib         ,"clusterECalib/F"          );
  m_clusterTree->Branch("clusterPtCalib"        ,&m_fClusterPtCalib        ,"clusterPtCalib/F"         );    
  m_clusterTree->Branch("clusterEtaCalib"       ,&m_fClusterEtaCalib       ,"clusterEtaCalib/F"        );    
  m_clusterTree->Branch("clusterPhiCalib"       ,&m_fClusterPhiCalib       ,"clusterPhiCalib/F"        );    
  m_clusterTree->Branch("cluster_sumCellECalib" ,&m_fCluster_sumCellECalib ,"cluster_sumCellECAlib/F"  );
  m_clusterTree->Branch("cluster_fracECalib"    ,&m_fCluster_fracECalib    ,"cluster_fracECalib/F"     );
  m_clusterTree->Branch("cluster_fracECalib_ref",&m_fCluster_fracECalib_ref,"cluster_fracECalib_ref/F" );

  // -- cluster kinematics: EM scale if doUnCalibratedClusters == true (default)
  m_clusterTree->Branch("clusterE"         ,&m_fClusterE         ,"clusterE/F"          );    
  m_clusterTree->Branch("clusterPt"        ,&m_fClusterPt        ,"clusterPt/F"         );    
  m_clusterTree->Branch("clusterEta"       ,&m_fClusterEta       ,"clusterEta/F"        );    
  m_clusterTree->Branch("clusterPhi"       ,&m_fClusterPhi       ,"clusterPhi/F"        );    
  m_clusterTree->Branch("cluster_sumCellE" ,&m_fCluster_sumCellE ,"cluster_sumCellE/F"  );
  m_clusterTree->Branch("cluster_time"     ,&m_fClusterTime      ,"cluster_time/F"      );
  m_clusterTree->Branch("cluster_fracE"    ,&m_fCluster_fracE    ,"cluster_fracE/F"     );
  m_clusterTree->Branch("cluster_fracE_ref",&m_fCluster_fracE_ref,"cluster_fracE_ref/F" );

  // -- cluster calibration: weights and true energies
  m_clusterTree->Branch("cluster_EM_PROBABILITY",     &m_fCluster_EM_PROBABILITY,     "cluster_EM_PROBABILITY/F"     ); // calibrated cluster!!
  m_clusterTree->Branch("cluster_HAD_WEIGHT",         &m_fCluster_HAD_WEIGHT,         "cluster_HAD_WEIGHT/F"         );
  m_clusterTree->Branch("cluster_OOC_WEIGHT",         &m_fCluster_OOC_WEIGHT,         "cluster_OOC_WEIGHT/F"         );
  m_clusterTree->Branch("cluster_DM_WEIGHT",          &m_fCluster_DM_WEIGHT,          "cluster_DM_WEIGHT/F"          );
  m_clusterTree->Branch("cluster_ENG_CALIB_TOT",      &m_fCluster_ENG_CALIB_TOT,      "cluster_ENG_CALIB_TOT/F"      );  
  m_clusterTree->Branch("cluster_ENG_CALIB_OUT_T",    &m_fCluster_ENG_CALIB_OUT_T,    "cluster_ENG_CALIB_OUT_T/F"    );
  m_clusterTree->Branch("cluster_ENG_CALIB_OUT_L",    &m_fCluster_ENG_CALIB_OUT_L,    "cluster_ENG_CALIB_OUT_L/F"    );
  m_clusterTree->Branch("cluster_ENG_CALIB_OUT_M",    &m_fCluster_ENG_CALIB_OUT_M,    "cluster_ENG_CALIB_OUT_M/F"    );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_T",   &m_fCluster_ENG_CALIB_DEAD_T,   "cluster_ENG_CALIB_DEAD_T/F"   );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_L",   &m_fCluster_ENG_CALIB_DEAD_L,   "cluster_ENG_CALIB_DEAD_L/F"   );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_M",   &m_fCluster_ENG_CALIB_DEAD_M,   "cluster_ENG_CALIB_DEAD_M/F"   );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_TOT", &m_fCluster_ENG_CALIB_DEAD_TOT, "cluster_ENG_CALIB_DEAD_TOT/F" );
  m_clusterTree->Branch("cluster_ENG_CALIB_FRAC_EM" , &m_fCluster_ENG_CALIB_FRAC_EM,  "cluster_ENG_CALIB_FRAC_EM/F"  );
  m_clusterTree->Branch("cluster_ENG_CALIB_FRAC_HAD" ,&m_fCluster_ENG_CALIB_FRAC_HAD, "cluster_ENG_CALIB_FRAC_HAD/F" );
  m_clusterTree->Branch("cluster_ENG_CALIB_FRAC_REST",&m_fCluster_ENG_CALIB_FRAC_REST,"cluster_ENG_CALIB_FRAC_REST/F"); 

  // -- cluster moments (defaults from previous studies 
  m_clusterTree->Branch("cluster_CENTER_MAG",        &m_fCluster_CENTER_MAG,        "cluster_CENTER_MAG/F"        );
  m_clusterTree->Branch("cluster_FIRST_ENG_DENS",    &m_fCluster_FIRST_ENG_DENS,    "cluster_FIRST_ENG_DENS/F"    );

  // -- cluster moments (dynamically configured)
  std::vector<std::string> defClusterMomentNames = { "CENTER_MAG", "EM_PROBABILITY", "FIRST_ENG_DENS" };
  std::vector<std::string> actClusterMomentNames;
  for ( const auto& mname : m_caloClusterMoments ) {
    if ( (m_knownClusterMomentType.find(mname) != m_knownClusterMomentType.end()) && 
	 (std::find(defClusterMomentNames.begin(),defClusterMomentNames.end(),mname) == defClusterMomentNames.end()) ) { actClusterMomentNames.push_back(mname); }
  }

  // -- cluster moment configuration report
  ATH_MSG_INFO( "Number of moments requested " << actClusterMomentNames.size() );
  std::string msg; char buffer1[128];
  size_t nwrds(0);
  for ( auto fiter(m_knownClusterMomentName.begin()); fiter != m_knownClusterMomentName.end(); ++fiter ) { 
    nwrds = std::max(fiter->second.length(),nwrds);
  }
  int ictr(0);
  for ( const auto& mom : actClusterMomentNames ) {
    if ( ictr == 4 ) {  ATH_MSG_INFO( "....." << msg ); ictr = 0; msg = ""; }
    sprintf(buffer1," %-*.*s",(int)nwrds,(int)nwrds,mom.c_str());
    msg += std::string(buffer1);
    ++ictr;
  } 
  if ( msg != "" ) { ATH_MSG_INFO( "....." << msg ); } 

  // -- cluster moments - set up dynamic moment cache 
  float* fpntr = (float*)0; // dummy load
  for ( const auto& mname : actClusterMomentNames ) { 
    xAOD::CaloCluster::MomentType cmtype  = m_knownClusterMomentType.at(mname);
    double                        cmscale = m_knownClusterMomentScale.at(cmtype);
    m_caloClusterMomentLink.push_back(std::make_tuple(cmtype,mname,fpntr,cmscale));
  }

  // -- cluster moments - book branches
  if ( !this->bookClusterMoments(nwrds) ) { ATH_MSG_WARNING("no cluster moments booked in tuple!"); } 

  // -- cell data
  m_cellTree->Branch("cellE"                 , &m_cellEem               , "cellE/F"                  );
  m_cellTree->Branch("cellEdep"              , &m_cellEdep              , "cellEdep/F"               );
  m_cellTree->Branch("cellEta"               , &m_cellEta               , "cellEta/F"                );
  m_cellTree->Branch("cellPhi"               , &m_cellPhi               , "cellPhi/F"                );
  m_cellTree->Branch("cellTime"              , &m_cellTime              , "cellTime/F"               );
  m_cellTree->Branch("cellQuality"           , &m_cellQuality           , "cellQuality/s"            );      
  m_cellTree->Branch("cellSampling"          , &m_cellSampling          , "cellSampling/s"           );
  m_cellTree->Branch("cellID"                , &m_cellID                , "cellID/i"                 );   
  m_cellTree->Branch("cellClusterLink"       , &m_cellClusterLink       , "cellClusterLink/L"        );
  m_cellTree->Branch("cellInCluster"         , &m_cellInCluster         , "cellInCluster/O"          );
  if ( m_addCellCartesian )  {
    m_cellTree->Branch("cellCenterX", &m_cellCenterX, "cellCenterX/F");
    m_cellTree->Branch("cellCenterY", &m_cellCenterY, "cellCenterY/F");
    m_cellTree->Branch("cellCenterZ", &m_cellCenterZ, "cellCenterZ/F");
  }
  m_cellTree->Branch("cellIsTile"            , &m_cellIsTile            , "cellIsTile/O"             );
  m_cellTree->Branch("cellIsLAr"             , &m_cellIsLAr             , "cellIsLAr/O"              );
  m_cellTree->Branch("cellIsLArFwd"          , &m_cellIsLArFwd          , "cellIsLArFwd/O"           );
  m_cellTree->Branch("cellIsLArHEC"          , &m_cellIsLArHEC          , "cellIsLArHEC/O"           );
  m_cellTree->Branch("cellSignificance"      , &m_cellSignificance      , "cellSignificance/F"       );
  // -- initialize cell truth tool
  // if ( m_calibHitToCaloCellTool.retrieve().isFailure() ) { 
  //   ATH_MSG_ERROR( "Failed to retrieve tool collecting deposited energies in cell at toolhandle " << m_calibHitToCaloCellTool ); 
  //   return StatusCode::FAILURE;
  // }

  // -- open debug stream file
  if ( m_debugStreamFlag ) {
    if ( m_debugStream.is_open() ) { m_debugStream.close(); }
    if ( m_debugStreamName == "" ) { ATH_MSG_ERROR( "debug stream is requested (flag = " << boolToName.at(m_debugStreamFlag) << ") but the stream name is empty/invalid"); return StatusCode::FAILURE; }  
    m_debugStream.open(m_debugStreamName); 
    if ( !m_debugStream.good() ) { 
      ATH_MSG_WARNING( "debug stream is requested (flag = " << boolToChar.at(m_debugStreamFlag) << ") but is in bad state: turning it off!"); m_debugStreamFlag = false;  
    } else {
      ATH_MSG_INFO   ( "debug stream is requested (flag = " << boolToChar.at(m_debugStreamFlag) << ") and sucessfully opened"              );
    }
  }

  return StatusCode::SUCCESS;

} // initialize()

/////////////
// Execute //
/////////////

StatusCode CellTreeMaker::execute() {  

  const EventContext& ctx = Gaudi::Hive::currentContext();

  char buffer[1024];
  static const std::string mname = "CellTreeMaker::execute()";

  // -- reset all variables
  this->resetAll();

  // -- principal event counter
  ++m_allEvntCtr;
  sprintf(buffer,"@BeginEvent: %i",m_allEvntCtr);
  resourceMessage(mname,std::string(buffer));

  // -- general event information
  SG::ReadHandle<xAOD::EventInfo> eventInfoHandle(m_eventInfoKey);
  if ( !eventInfoHandle.isValid()) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::EventInfo object with key <" << eventInfoHandle.key() << ">" ); return StatusCode::FAILURE; }
  debugMessage(mname,"#--- created handle \042%s\042",eventInfoHandle.key().c_str());

  // -- truth particles
  SG::ReadHandle<xAOD::TruthParticleContainer> truthParticleContainerHandle(m_truthParticleContainerKey);
  if ( !truthParticleContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::TruthParticleContainer object with key <" << truthParticleContainerHandle.key() << ">" ); return StatusCode::FAILURE; }

  // -- reco calorimeter event (cell level) 
  SG::ReadHandle<CaloCellContainer> cellContainerHandle(m_cellContainerKey); 
  if ( !cellContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to CaloCellContainer object with key <" << cellContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
  m_recoCellContPtr = cellContainerHandle.cptr(); 

  // -- cell significance
  SG::ReadCondHandle<CaloNoise> noiseHdl(m_noiseCDOKey);
  if (!noiseHdl.isValid()) { ATH_MSG_ERROR( "cannot retrieve handle to CaloCellContainer object with key <" << cellContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
  m_noise = *noiseHdl;
  
  // -- calo cluster container
  SG::ReadHandle<xAOD::CaloClusterContainer> clusterContainerHandle(m_clusterContainerKey); 
  if ( !clusterContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::CaloClusterContainer object with key <" << clusterContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
  debugMessage(mname,"#--- created handle \042%s\042",clusterContainerHandle.key().c_str());
  
  // -- truth calorimeter event (cell level) -> this produces the cellTruthContainer
  // ATH_CHECK( m_calibHitToCaloCellTool->processCalibHitsFromParticle(truthParticleContainerHandle->at(0)->barcode()) );
  // SG::ReadHandle<CaloCellContainer> cellTruthContainerHandle(m_cellTruthContainerKey);
  // if ( !cellTruthContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to CaloCellContainer object with key <" << cellTruthContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
  // m_truthCellContPtr = cellTruthContainerHandle.cptr();
  // -- collect some event information
  m_runNumber   = eventInfoHandle->runNumber();
  m_eventNumber = eventInfoHandle->eventNumber();
  debugMessage(mname,"# --- run number %08i event number %08lli",m_runNumber,m_eventNumber);
  m_averageMu = eventInfoHandle->actualInteractionsPerCrossing(); 
  SG::ReadHandle<xAOD::VertexContainer> vertexContainerHandle(m_vertexContainerKey);
  if ( !vertexContainerHandle.isValid() ) { 
    ATH_MSG_WARNING( "cannot retrieve vertex container with key <" << vertexContainerHandle.key() << ">, assume one primary vertex" ); 
    m_nPrimVtx = 1;
  } else { 
    for ( auto pVtx : *(vertexContainerHandle.cptr()) ) { if ( pVtx->vertexType() == xAOD::VxType::PriVtx || pVtx->vertexType() == xAOD::VxType::PileUp ) { ++m_nPrimVtx; } }
    debugMessage(mname,"# ----- [jets/particles] number of primary vertices %3i mu = %4.1f",m_nPrimVtx,m_averageMu);
  }

  // -- fill truth particle
  if( truthParticleContainerHandle->empty() ) { 
    m_fClusterTruthE   = -1.;
    m_fClusterTruthPDG = 0;
    return StatusCode::SUCCESS; 
  }
  m_fClusterTruthE   = truthParticleContainerHandle->at(0)->e() /Gaudi::Units::GeV;
  m_fClusterTruthPt  = truthParticleContainerHandle->at(0)->pt()/Gaudi::Units::GeV;
  m_fClusterTruthEta = truthParticleContainerHandle->at(0)->eta();
  m_fClusterTruthPhi = truthParticleContainerHandle->at(0)->phi();
  m_fClusterTruthPDG = truthParticleContainerHandle->at(0)->pdgId();
  m_barCode          = truthParticleContainerHandle->at(0)->barcode();

  // -- calorimeter truth (needs barcode!)
  if ( !loadDepositedEnergy() ) { ATH_MSG_ERROR( "cannot load deposited energies for event " << m_runNumber << "/" << m_eventNumber ); return StatusCode::FAILURE; }

  // -- fill clusters
  ++m_fParticleCount; 
  std::vector<bool> cellsUsed(cellContainerHandle->size(),false);
  ATH_MSG_DEBUG( "cell container size (handle/pointer) " << cellContainerHandle->size() << "/" << m_recoCellContPtr->size() << " flag vector size " << cellsUsed.size() );
  m_cellInCluster = false;
  auto particleIdx (m_clusterTree->GetEntries());
  if ( m_useClusteredCells ) { if ( !fillClusters<xAOD::CaloClusterContainer>(clusterContainerHandle.cptr(),m_fClusterTruthE,cellsUsed) && m_fClusterTruthPDG > 0  ) { m_cellClusterLink = particleIdx; } }
  m_cellInCluster = false;
  m_cellClusterLink = particleIdx;
  fillCells<CaloCellContainer>(cellContainerHandle.cptr(),cellsUsed,m_useAllCells,true);
  return StatusCode::SUCCESS;
} // execute()

StatusCode CellTreeMaker::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  
  if ( m_allEvntCtr > 0 ) {
    // find number of digits (up to 12)
    int nmax = std::min(std::max( { m_allEvntCtr, m_allClusCtr, 1 } ),12);
    int ndig = std::min(std::max(((int)std::log10((double)nmax))+3,1),12);
    // format output
    char buffer[2048];
    sprintf(buffer,"total # events .....: %*i accepted # events .....: %*i -> %6.2f%%",ndig,m_allEvntCtr,ndig,m_accEvntCtr,((double)m_accEvntCtr)/((double)m_allEvntCtr)*100.);
    ATH_MSG_INFO( buffer );
    // all clusters
    if ( m_allClusCtr > 0 ) { 
      sprintf(buffer,"total # clusters ...: %*i accepted # clusters ...: %*i -> %6.2f%%",ndig,m_allClusCtr,ndig,m_accClusCtr,((double)m_accClusCtr)/((double)m_allClusCtr)*100.);
    } else { 
      sprintf(buffer,"total # clusters ...: %*i accepted # clusters ...: %*i",ndig,m_allClusCtr,ndig,m_accClusCtr);
    }
    ATH_MSG_INFO( buffer );
    // per accepted event stats
    sprintf(buffer,"<#clusters/event> ..: %*.3f (accepted events only)",ndig,((double)m_accClusCtr)/((double)m_accEvntCtr));
    ATH_MSG_INFO( buffer ); 
  }

  return StatusCode::SUCCESS;
}

void CellTreeMaker::resetClusterMomentLinkValue(float value) { 
  for ( auto& mlnk : m_caloClusterMomentLink ) {
    if ( std::get<2>(mlnk) != 0 ) { *(std::get<2>(mlnk)) = value; } 
  }
}

void CellTreeMaker::registerClusterMoment(MomentLink& mlink) { 
  static std::string _prefix = "cluster_";
  static std::string _vtype  = "/F";
  std::string bname(_prefix+std::get<1>(mlink));
  float*      baddr(std::get<2>(mlink)); 
  std::string bdesc(bname+_vtype);
  m_clusterTree->Branch(bname.c_str(),baddr,bdesc.c_str());
}

bool CellTreeMaker::bookClusterMoments(size_t nwrds) {
  char buffer[256];
  // book branches for requested moments
  int lwrds((int)nwrds);
  for ( auto& mlnk : m_caloClusterMomentLink ) { 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_MAG       ) { std::get<2>(mlnk) = &m_fCluster_CENTER_MAG       ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::FIRST_PHI        ) { std::get<2>(mlnk) = &m_fCluster_FIRST_PHI        ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::FIRST_ETA        ) { std::get<2>(mlnk) = &m_fCluster_FIRST_ETA        ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_R         ) { std::get<2>(mlnk) = &m_fCluster_SECOND_R         ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_LAMBDA    ) { std::get<2>(mlnk) = &m_fCluster_SECOND_LAMBDA    ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::DELTA_PHI        ) { std::get<2>(mlnk) = &m_fCluster_DELTA_PHI        ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::DELTA_THETA      ) { std::get<2>(mlnk) = &m_fCluster_DELTA_THETA      ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::DELTA_ALPHA      ) { std::get<2>(mlnk) = &m_fCluster_DELTA_ALPHA      ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_X         ) { std::get<2>(mlnk) = &m_fCluster_CENTER_X         ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_Y         ) { std::get<2>(mlnk) = &m_fCluster_CENTER_Y         ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_Z         ) { std::get<2>(mlnk) = &m_fCluster_CENTER_Z         ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_LAMBDA    ) { std::get<2>(mlnk) = &m_fCluster_CENTER_LAMBDA    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::LATERAL          ) { std::get<2>(mlnk) = &m_fCluster_LATERAL          ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::LONGITUDINAL     ) { std::get<2>(mlnk) = &m_fCluster_LONGITUDINAL     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_EM      ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_EM      ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_MAX     ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_MAX     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_CORE    ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_CORE    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::FIRST_ENG_DENS   ) { std::get<2>(mlnk) = &m_fCluster_FIRST_ENG_DENS   ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_ENG_DENS  ) { std::get<2>(mlnk) = &m_fCluster_SECOND_ENG_DENS  ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ISOLATION        ) { std::get<2>(mlnk) = &m_fCluster_ISOLATION        ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_BAD_CELLS    ) { std::get<2>(mlnk) = &m_fCluster_ENG_BAD_CELLS    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::N_BAD_CELLS      ) { std::get<2>(mlnk) = &m_fCluster_N_BAD_CELLS      ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::N_BAD_CELLS_CORR ) { std::get<2>(mlnk) = &m_fCluster_N_BAD_CELLS_CORR ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::BAD_CELLS_CORR_E ) { std::get<2>(mlnk) = &m_fCluster_BAD_CELLS_CORR_E ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::BADLARQ_FRAC     ) { std::get<2>(mlnk) = &m_fCluster_BADLARQ_FRAC     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_POS          ) { std::get<2>(mlnk) = &m_fCluster_ENG_POS          ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SIGNIFICANCE     ) { std::get<2>(mlnk) = &m_fCluster_SIGNIFICANCE     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CELL_SIGNIFICANCE) { std::get<2>(mlnk) = &m_fCluster_CELL_SIGNIFICANCE; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CELL_SIG_SAMPLING) { std::get<2>(mlnk) = &m_fCluster_CELL_SIG_SAMPLING; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::AVG_LAR_Q        ) { std::get<2>(mlnk) = &m_fCluster_AVG_LAR_Q        ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::AVG_TILE_Q       ) { std::get<2>(mlnk) = &m_fCluster_AVG_TILE_Q       ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_BAD_HV_CELLS ) { std::get<2>(mlnk) = &m_fCluster_ENG_BAD_HV_CELLS ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::N_BAD_HV_CELLS   ) { std::get<2>(mlnk) = &m_fCluster_N_BAD_HV_CELLS   ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::PTD              ) { std::get<2>(mlnk) = &m_fCluster_PTD              ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::MASS             ) { std::get<2>(mlnk) = &m_fCluster_MASS             ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::EM_PROBABILITY   ) { std::get<2>(mlnk) = &m_fCluster_EM_PROBABILITY   ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_TIME      ) { std::get<2>(mlnk) = &m_fCluster_SECOND_TIME      ; this->registerClusterMoment(mlnk); }
    // print
    sprintf(buffer,"moment: %-*.*s enumerator: %i6/0x%04x address: %p scale: %.3f",
	    lwrds,lwrds,std::get<1>(mlnk).c_str(),
	    (int)std::get<0>(mlnk),
	    (unsigned int)std::get<0>(mlnk),
	    (void*)std::get<2>(mlnk),
	    std::get<3>(mlnk));
    ATH_MSG_INFO( "Booked - " << buffer );
  }
  return !m_caloClusterMomentLink.empty();
}

bool CellTreeMaker::applyFilter(const xAOD::CaloCluster& rclus,xAOD::CaloCluster::State s) {
  // truth-based selection
  double edep(0.); if ( rclus.retrieveMoment(xAOD::CaloCluster::ENG_CALIB_TOT,edep) && (edep<m_clusterTruthEmin || edep>m_clusterTruthEmax) ) { return false; }
  // signal-based selection
  xAOD::CaloCluster::State scale = m_useLCWScaleAsReference ? xAOD::CaloCluster::CALIBRATED : s; 
  return ( rclus.e(scale) > m_clusterEmin && rclus.e(scale) < m_clusterEmax ) && ( rclus.eta(scale) > m_clusterRapMin && rclus.eta(scale) < m_clusterRapMax );
} 

bool CellTreeMaker::resetAll() {
  // -- event info
  m_runNumber   = 0;
  m_eventNumber = 0;
  m_nPrimVtx    = 0;
  m_averageMu   = 0.;
  // -- single particles
  m_fClusterTruthE   = 0.;
  m_fClusterTruthPt  = 0.;
  m_fClusterTruthEta = 0.;
  m_fClusterTruthPhi = 0.;
  m_fClusterTruthPDG = 0 ;
  // -- cluster specs
  m_nCluster      = 0;
  m_fClusterIndex = 0;
  // cell specs
  m_cellClusterLink  = -1;
  m_cellInCluster    = false; 
  // -- reset clusters
  return resetClusters();
}
bool CellTreeMaker::resetClusters() {
  // -- cluster specs
  m_fCluster_nCells     = 0; // # cells with E > 0
  m_fCluster_nCells_tot = 0; // # all cells
  m_fCluster_lastCell  
 = -1;
  m_fCluster_lastCell   = -1;
  // -- cluster signals: E0 scale (optional use)
  m_fClusterE          = 0.;
  m_fClusterPt         = 0.;
  m_fClusterEta        = 0.;
  m_fClusterPhi        = 0.;
  m_fCluster_sumCellE  = 0.;
  m_fClusterTime       = 0.;
  m_fCluster_fracE     = 0.; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  m_fCluster_fracE_ref = 0.; // fractional contribution particle (cluster_energy/reference energy)   
  // -- cluster signals: LCW scale (default/always use)
  m_fClusterECalib          = 0.;
  m_fClusterPtCalib         = 0.;
  m_fClusterEtaCalib        = 0.;
  m_fClusterPhiCalib        = 0.;
  m_fCluster_sumCellECalib  = 0.;
  m_fCluster_fracECalib     = 0.; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  m_fCluster_fracECalib_ref = 0.; // fractional contribution particle (cluster_energyreference energy)   
  // -- cluster truth info
  m_fCluster_ENG_CALIB_TOT       = 0.;   // energy deposit in cluster cells 
  m_fCluster_ENG_CALIB_OUT_T     = 0.;   // energy deposit outside of cluster but associated (tight)
  m_fCluster_ENG_CALIB_OUT_L     = 0.;   // energy deposit outside of cluster but associated (loose)
  m_fCluster_ENG_CALIB_OUT_M     = 0.;   // energy deposit outside of cluster but associated (medium)
  m_fCluster_ENG_CALIB_DEAD_T    = 0.;   // dead material energy deposit associated with cluster (tight)
  m_fCluster_ENG_CALIB_DEAD_L    = 0.;   // dead material energy deposit associated with cluster (loose)
  m_fCluster_ENG_CALIB_DEAD_M    = 0.;   // dead material energy deposit associated with cluster (medium)
  m_fCluster_ENG_CALIB_DEAD_TOT  = 0.;   // dead material energy deposit associated with cluster (all)
  m_fCluster_ENG_CALIB_FRAC_EM   = 0.;   // energy deposited by electromagnetic processes
  m_fCluster_ENG_CALIB_FRAC_HAD  = 0.;   // energy depoisted by hadronic processes/ionizations
  m_fCluster_ENG_CALIB_FRAC_REST = 0.;   // energy deposited by all other processes (unspecific in Geant4)
  // -- calibration weights
  m_fCluster_EM_PROBABILITY = 0.;       // EM likelihood
  m_fCluster_HAD_WEIGHT     = 0.;       // hadronic weights sum(w_cell x E_cell)/sum(E_cell)
  m_fCluster_OOC_WEIGHT     = 0.;       // out-of-cluster correction factor (OOC)
  m_fCluster_DM_WEIGHT      = 0.;       // dead material correction factor (DMC)
  m_fCluster_Ehad           = 0.;       // hadronic calibration applied:  HAD_WEIGHT * ClusterE
  m_fCluster_Eooc           = 0.;       // amount of energy added by OOC: (1-OOC_WEIGHT)*Ehad 
  m_fCluster_Edmc           = 0.;       // amount of energy added by DMC: (1-DM_WEIGHT)*(Ehad+Eooc)
  // -- cluster moments (used as configured)
  m_fCluster_CENTER_MAG        = 0.;    // always used (historic)
  m_fCluster_FIRST_PHI         = 0.;
  m_fCluster_FIRST_ETA         = 0.;
  m_fCluster_SECOND_R          = 0.;
  m_fCluster_SECOND_LAMBDA     = 0.;
  m_fCluster_DELTA_PHI         = 0.;
  m_fCluster_DELTA_THETA       = 0.;
  m_fCluster_DELTA_ALPHA       = 0.;
  m_fCluster_CENTER_X          = 0.;
  m_fCluster_CENTER_Y          = 0.;
  m_fCluster_CENTER_Z          = 0.;
  m_fCluster_CENTER_LAMBDA     = 0.;
  m_fCluster_LATERAL           = 0.;
  m_fCluster_LONGITUDINAL      = 0.;
  m_fCluster_ENG_FRAC_EM       = 0.;
  m_fCluster_ENG_FRAC_MAX      = 0.;
  m_fCluster_ENG_FRAC_CORE     = 0.;
  m_fCluster_FIRST_ENG_DENS    = 0.;    // always used (historic)
  m_fCluster_SECOND_ENG_DENS   = 0.;
  m_fCluster_ISOLATION         = 0.;
  m_fCluster_ENG_BAD_CELLS     = 0.;
  m_fCluster_N_BAD_CELLS       = 0.;
  m_fCluster_N_BAD_CELLS_CORR  = 0.;
  m_fCluster_BAD_CELLS_CORR_E  = 0.;
  m_fCluster_BADLARQ_FRAC      = 0.;
  m_fCluster_ENG_POS           = 0.;
  m_fCluster_SIGNIFICANCE      = 0.;
  m_fCluster_CELL_SIGNIFICANCE = 0.;
  m_fCluster_CELL_SIG_SAMPLING = 0.;
  m_fCluster_AVG_LAR_Q         = 0.;
  m_fCluster_AVG_TILE_Q        = 0.;
  m_fCluster_ENG_BAD_HV_CELLS  = 0.;
  m_fCluster_N_BAD_HV_CELLS    = 0.;
  m_fCluster_PTD               = 0.;
  m_fCluster_MASS              = 0.;
  m_fCluster_SECOND_TIME       = 0.;
  return true; 
}

bool CellTreeMaker::resetCells() {
  m_cellSignificance = 0.;
  m_cellEdep         = 0.; // deposited energy in cell
  m_cellEem          = 0.; // EM scale energy in cell
  m_cellEta          = 0.; // cell rapidity
  m_cellPhi          = 0.; // cell azimuth
  m_cellTime         = 0.; // cell time
  m_cellQuality      = 0 ; // cell quality
  m_cellSampling     = 0 ; // cell sampling
  return true; 
}

void CellTreeMaker::debugMessage(const std::string& module,const std::string& fmtStr,...) {
  // some constants
  static constexpr int                    nsize  = 2048; 
  static const     std::string::size_type swidth = CTM_MNAME_W ;
  static const     std::string            dots   = "...";
  // static constexpr std::string            lbrak  = "[";
  // static constexpr std::string            rbrak  = "]";
  // buffer
  char buffer[nsize];
  // -- check if debug stream is active
  if ( !m_debugStreamFlag || !m_debugStream.good() ) { return; }
  // -- adapt string for display
  std::string modn(module);
  if ( modn.length() > swidth  ) { modn.resize(swidth-dots.length()); modn += dots; }
  int npos(sprintf(&buffer[0],"[%-*.*s][%6i] ",(int)swidth,(int)swidth,modn.c_str(),m_allEvntCtr));
  // add variable argument list to displayed string
  va_list args; 
  va_start(args,fmtStr); 
  npos += vsnprintf(&buffer[npos],nsize,fmtStr.c_str(),args); 
  va_end(args);
  // print message and flush (for debugging)
  m_debugStream << buffer << std::endl << std::flush;
}

void CellTreeMaker::resourceMessage(const std::string& module,const std::string& tag) { 
  static const int nsize = 512;
  static char buffer[nsize];
  sprintf(buffer,"%s --> |- vmem %s -|- pmem %s -|",tag.c_str(),Resource::virtualMemory(Resource::Unit::kB,true).c_str(),Resource::physicalMemory(Resource::Unit::kB,true).c_str());
  debugMessage(module,std::string(buffer));
}

int CellTreeMaker::fillDepositedEnergy(const CaloCalibrationHitContainer& ccont,const CaloDetDescrManager* caloDDM,celldeposit_container_t& collectedEnergies) {
  int ncells(0);
  for ( const auto chptr : ccont ) { 
    // -- only required barcodes
    // ATH_MSG_INFO( "barcode in file " << chptr->particleID() << " barcode requested " << m_barCode );  
    if ( chptr->particleID() != static_cast<decltype(chptr->particleID())>(m_barCode)          ) { continue; }
    // -- check if inside calorimeter
    // ATH_MSG_INFO( "LAr cell " << boolToChar.at(m_caloCell_ID->is_lar(chptr->cellID())) << " Tile cell " << boolToChar.at(m_caloCell_ID->is_tile(chptr->cellID())) ); 
    if (  !(m_caloCell_ID->is_lar(chptr->cellID()) || m_caloCell_ID->is_tile(chptr->cellID())) ) { continue; }
    // -- pick up calorimeter description element  
    const CaloDetDescrElement* cde = caloDDM->get_element(chptr->cellID());
    if ( cde == nullptr ) { ATH_MSG_WARNING("invalid pointer to calorimeter detector description element for calo cell ID " << chptr->cellID()); ncells = -1; break; }
    // -- accumulate energies (collect active and inactive deposits in each cell)
    int cellidx(m_recoCellContPtr->findIndex(cde->calo_hash())); 
    if ( cellidx >= 0 && static_cast<size_t>(cellidx) < collectedEnergies.size() ) {
      std::get<0>(collectedEnergies[cellidx]) += chptr->energyTotal(); // total energy deposit in inactive and active layers of cell
      std::get<1>(collectedEnergies[cellidx]) += chptr->energy(0);     // EM deposited energy 
      std::get<2>(collectedEnergies[cellidx]) += chptr->energy(1);     // non-EM deposited energy
      ++ncells;
    } // accumulation of energy
  } // loop hits
  return ncells;
}

bool CellTreeMaker::loadDepositedEnergy() { 
  // check if cache is the correct size and/or reset cache
  if ( m_cellTruthEnergies.size() < m_recoCellContPtr->size() ) { 
    m_cellTruthEnergies.resize(m_recoCellContPtr->size(),{ 0., 0., 0. });  
  } else { 
    std::tuple<double,double,double> emptyData = { 0., 0., 0. };
    std::fill(m_cellTruthEnergies.begin(),m_cellTruthEnergies.end(), emptyData ); 
  }
  // allocate calo DDM
  const CaloDetDescrManager* caloDDM = CaloDetDescrManager::instance(); 
  // collect deposited energies
  // SG::ReadCondHandle<CaloDetDescrManager>     caloDescrHandle (m_caloDetDescrMgrKey     ); if ( !caloDescrHandle.isValid()  ) { ATH_MSG_ERROR( "cannot allocate CaloDetDescrManager with key <" << m_caloDetDescrMgrKey << ">" ); return false; }
  SG::ReadHandle<CaloCalibrationHitContainer> larActiveHits   (m_activeCalibHitLArKey   ); if ( !larActiveHits.isValid()    ) { ATH_MSG_ERROR( "cannot allocate CalibrationHitContainer with key <" << larActiveHits.key()    << ">" ); return false; }
  SG::ReadHandle<CaloCalibrationHitContainer> larInactiveHits (m_inactiveCalibHitLArKey ); if ( !larInactiveHits.isValid()  ) { ATH_MSG_ERROR( "cannot allocate CalibrationHitContainer with key <" << larInactiveHits.key()  << ">" ); return false; }
  SG::ReadHandle<CaloCalibrationHitContainer> tileActiveHits  (m_activeCalibHitTileKey  ); if ( !tileActiveHits.isValid()   ) { ATH_MSG_ERROR( "cannot allocate CalibrationHitContainer with key <" << tileActiveHits.key()   << ">" ); return false; }
  SG::ReadHandle<CaloCalibrationHitContainer> tileInactiveHits(m_inactiveCalibHitTileKey); if ( !tileInactiveHits.isValid() ) { ATH_MSG_ERROR( "cannot allocate CalibrationHitContainer with key <" << tileInactiveHits.key() << ">" ); return false; }
  // collect energies from containers
  int ncells(0); int kcells(0);
  if ( (kcells = fillDepositedEnergy(*larActiveHits   ,caloDDM,m_cellTruthEnergies)) < 0 ) { ATH_MSG_WARNING( "fillDepositedEnergy returns " << kcells << " for LAr active hits"    ); return false; } else { ncells += kcells; } 
  if ( (kcells = fillDepositedEnergy(*larInactiveHits ,caloDDM,m_cellTruthEnergies)) < 0 ) { ATH_MSG_WARNING( "fillDepositedEnergy returns " << kcells << " for LAr incative hits"  ); return false; } else { ncells += kcells; }
  if ( (kcells = fillDepositedEnergy(*tileActiveHits  ,caloDDM,m_cellTruthEnergies)) < 0 ) { ATH_MSG_WARNING( "fillDepositedEnergy returns " << kcells << " for Tile active hits"   ); return false; } else { ncells += kcells; }
  if ( (kcells = fillDepositedEnergy(*tileInactiveHits,caloDDM,m_cellTruthEnergies)) < 0 ) { ATH_MSG_WARNING( "fillDepositedEnergy returns " << kcells << " fot Tile inactive hits" ); return false; } else { ncells += kcells; }
  // there may be no hits i calorimeter for given (low energy) particle
  return ncells >= 0; 
}

bool CellTreeMaker::acceptCell(const CaloCell& cell,std::vector<bool>& cellsUsed) const {
  auto fcidx(caloCellIndex(cell)); 
  if ( fcidx >= 0 && static_cast<size_t>(fcidx) < cellsUsed.size() ) {
    if ( cellsUsed.at(fcidx) ) { 
      return false; 
    } else { 
      cellsUsed[fcidx] = true; 
      return true;
    }
  } else { 
    ATH_MSG_WARNING("cell index for usage tracking out of range: " << fcidx << " with max index " << cellsUsed.size() );
    return false;  
  }
}
				       		        
bool CellTreeMaker::storeCell(const CaloCell& cell,std::vector<bool>& cellsUsed,bool rejectIfUsed) {
  bool cellCheck(acceptCell(cell,cellsUsed)); 
  if ( !rejectIfUsed || ( rejectIfUsed && cellCheck ) ) { storeCell(cell); return true; } else { return false; } 
}
		        				       		        
bool CellTreeMaker::storeCell(const CaloCell& cell) {  
  m_cellSignificance = m_noise != nullptr ? cell.energy()/m_noise->getEffectiveSigma(cell.ID(), cell.gain(), cell.energy()) : 0.;
  m_cellEem       = cell.energy()/Gaudi::Units::GeV; 
  m_cellEta       = cell.eta(); 
  m_cellPhi       = cell.phi()/Gaudi::Units::rad; 
  m_cellTime      = cell.time()/Gaudi::Units::ns; 
  m_cellQuality   = cell.quality(); 
  m_cellSampling  = cell.caloDDE()->getSampling(); 
  m_cellEdep      = depositedEnergy(cell)/Gaudi::Units::GeV;
  m_cellCenterX   = cell.x()/Gaudi::Units::millimeter;
  m_cellCenterY   = cell.y()/Gaudi::Units::millimeter;
  m_cellCenterZ   = cell.z()/Gaudi::Units::millimeter;
  m_cellID        = cell.caloDDE()->calo_hash(); 
  m_cellIsTile    = cell.caloDDE()->is_tile();
  m_cellIsLAr     = cell.caloDDE()->is_lar_em();
  m_cellIsLArFwd  = cell.caloDDE()->is_lar_fcal();
  m_cellIsLArHEC    = cell.caloDDE()->is_lar_hec();

  m_cellTree->Fill();  
  return true; 
}		        
