#include "MLTree/ClusterTreeMaker.h"

// Gaudi stuff
#include <GaudiKernel/MsgStream.h>

// StoreGate stuff
#include "StoreGate/ReadHandle.h"

// Utilities
#include "FourMomUtils/xAODP4Helpers.h"
#include "MLTree/SystemResources.h"

// STL
#include <string>
#include <vector>
#include <map>         // for multimap
#include <algorithm>
#include <memory>
#include <tuple>

// C headers
#include <cmath>
#include <cstdio>
#include <cstdarg>

// C I/O
#include <iostream>

#define  TYPE_ENTRY( NAME ) { #NAME , xAOD::CaloCluster:: NAME }
#define  NAME_ENTRY( NAME ) { xAOD::CaloCluster:: NAME , #NAME }

#ifndef CTM_MNAME_W
#define CTM_MNAME_W 32
#endif

namespace {
  static const std::map<bool,std::string> boolToName                      = { { true, "true"                      }, { false, "false"                      } };
  static const std::map<bool,const char*> boolToChar                      = { { true, boolToName.at(true).c_str() }, { false, boolToName.at(false).c_str() } };
  static const std::map<std::string,bool> nameToBool                      = { { "true", true                      }, { "false", false                      } };
  static const std::map<bool,std::string> boolToScale                     = { { true, "LCW"                       }, { false, "EM"                         } };
  static const std::map<std::string,bool> scaleToBool                     = { { "LCW", true                       }, { "EM", false                         } };
  static const std::map<xAOD::CaloCluster::State,std::string> enumToScale = { { xAOD::CaloCluster::UNCALIBRATED, "EM" }, { xAOD::CaloCluster::CALIBRATED, "LCW" }, { xAOD::CaloCluster::ALTCALIBRATED, "ALT" }, { xAOD::CaloCluster::UNKNOWN, "UNKNONW" } };
  static const std::map<std::string,xAOD::CaloCluster::State> scaleToEnum = { { "EM", xAOD::CaloCluster::UNCALIBRATED }, { "LCW", xAOD::CaloCluster::CALIBRATED }, {"ALT",  xAOD::CaloCluster::ALTCALIBRATED }, { "UNKNOWN", xAOD::CaloCluster::UNKNOWN } };
  static const std::map<StatusCode,std::string>               codeToName  = { { StatusCode::SUCCESS, "success" }, { StatusCode::FAILURE, "failed_" }, { StatusCode::RECOVERABLE, "recover" } };
}   

//////////////////
// Dictionaries //
//////////////////

// -- name-to-enum: std::string -> xAOD::CaloCluster::MomentType
std::map<std::string,xAOD::CaloCluster::MomentType> ClusterTreeMaker::m_knownClusterMomentType = { 
  TYPE_ENTRY( CENTER_MAG        ),
  TYPE_ENTRY( FIRST_PHI         ),
  TYPE_ENTRY( FIRST_ETA         ),
  TYPE_ENTRY( SECOND_R          ),
  TYPE_ENTRY( SECOND_LAMBDA     ),
  TYPE_ENTRY( DELTA_PHI         ),
  TYPE_ENTRY( DELTA_THETA       ),
  TYPE_ENTRY( DELTA_ALPHA       ),
  TYPE_ENTRY( CENTER_X          ),
  TYPE_ENTRY( CENTER_Y          ),
  TYPE_ENTRY( CENTER_Z          ),
  TYPE_ENTRY( CENTER_LAMBDA     ),
  TYPE_ENTRY( LATERAL           ),
  TYPE_ENTRY( LONGITUDINAL      ),
  TYPE_ENTRY( ENG_FRAC_EM       ),
  TYPE_ENTRY( ENG_FRAC_MAX      ),
  TYPE_ENTRY( ENG_FRAC_CORE     ),
  TYPE_ENTRY( ENG_FRAC_TIME     ),
  TYPE_ENTRY( CELL_TIME_MAX     ),
  TYPE_ENTRY( FIRST_ENG_DENS    ),
  TYPE_ENTRY( SECOND_ENG_DENS   ),
  TYPE_ENTRY( ISOLATION         ),
  TYPE_ENTRY( ENG_BAD_CELLS     ),
  TYPE_ENTRY( N_BAD_CELLS       ),
  TYPE_ENTRY( N_BAD_CELLS_CORR  ),
  TYPE_ENTRY( BAD_CELLS_CORR_E  ),
  TYPE_ENTRY( BADLARQ_FRAC      ),
  TYPE_ENTRY( ENG_POS           ),
  TYPE_ENTRY( SIGNIFICANCE      ),
  TYPE_ENTRY( CELL_SIGNIFICANCE ),
  TYPE_ENTRY( CELL_SIG_SAMPLING ),
  TYPE_ENTRY( AVG_LAR_Q         ),
  TYPE_ENTRY( AVG_TILE_Q        ),
  TYPE_ENTRY( ENG_BAD_HV_CELLS  ),
  TYPE_ENTRY( N_BAD_HV_CELLS    ),
  TYPE_ENTRY( PTD               ),
  TYPE_ENTRY( MASS              ),
  TYPE_ENTRY( EM_PROBABILITY    ),
  TYPE_ENTRY( SECOND_TIME       )
};

// -- enum to name: xAOD::CaloCluster::MomentType -> std::string 
std::map<xAOD::CaloCluster::MomentType,std::string> ClusterTreeMaker::m_knownClusterMomentName = { 
  NAME_ENTRY( CENTER_MAG        ),
  NAME_ENTRY( FIRST_PHI         ),
  NAME_ENTRY( FIRST_ETA         ),
  NAME_ENTRY( SECOND_R          ),
  NAME_ENTRY( SECOND_LAMBDA     ),
  NAME_ENTRY( DELTA_PHI         ),
  NAME_ENTRY( DELTA_THETA       ),
  NAME_ENTRY( DELTA_ALPHA       ),
  NAME_ENTRY( CENTER_X          ),
  NAME_ENTRY( CENTER_Y          ),
  NAME_ENTRY( CENTER_Z          ),
  NAME_ENTRY( CENTER_LAMBDA     ),
  NAME_ENTRY( LATERAL           ),
  NAME_ENTRY( LONGITUDINAL      ),
  NAME_ENTRY( ENG_FRAC_EM       ),
  NAME_ENTRY( ENG_FRAC_MAX      ),
  NAME_ENTRY( ENG_FRAC_CORE     ),
  NAME_ENTRY( ENG_FRAC_TIME     ),
  NAME_ENTRY( CELL_TIME_MAX     ),
  NAME_ENTRY( FIRST_ENG_DENS    ),
  NAME_ENTRY( SECOND_ENG_DENS   ),
  NAME_ENTRY( ISOLATION         ),
  NAME_ENTRY( ENG_BAD_CELLS     ),
  NAME_ENTRY( N_BAD_CELLS       ),
  NAME_ENTRY( N_BAD_CELLS_CORR  ),
  NAME_ENTRY( BAD_CELLS_CORR_E  ),
  NAME_ENTRY( BADLARQ_FRAC      ),
  NAME_ENTRY( ENG_POS           ),
  NAME_ENTRY( SIGNIFICANCE      ),
  NAME_ENTRY( CELL_SIGNIFICANCE ),
  NAME_ENTRY( CELL_SIG_SAMPLING ),
  NAME_ENTRY( AVG_LAR_Q         ),
  NAME_ENTRY( AVG_TILE_Q        ),
  NAME_ENTRY( ENG_BAD_HV_CELLS  ),
  NAME_ENTRY( N_BAD_HV_CELLS    ),
  NAME_ENTRY( PTD               ),
  NAME_ENTRY( MASS              ),
  NAME_ENTRY( EM_PROBABILITY    ),
  NAME_ENTRY( SECOND_TIME       )
};

// -- scale assignment
std::map<xAOD::CaloCluster::MomentType,double> ClusterTreeMaker::m_knownClusterMomentScale = { 
  { xAOD::CaloCluster::CENTER_MAG         , 1.0                          },
  { xAOD::CaloCluster::FIRST_PHI          , 1.0                          },
  { xAOD::CaloCluster::FIRST_ETA          , 1.0                          },
  { xAOD::CaloCluster::SECOND_R           , 1./Gaudi::Units::millimeter2 },
  { xAOD::CaloCluster::SECOND_LAMBDA      , 1./Gaudi::Units::millimeter2 },
  { xAOD::CaloCluster::DELTA_PHI          , 1./Gaudi::Units::radian      },
  { xAOD::CaloCluster::DELTA_THETA        , 1./Gaudi::Units::radian      },
  { xAOD::CaloCluster::DELTA_ALPHA        , 1./Gaudi::Units::radian      },
  { xAOD::CaloCluster::CENTER_X           , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::CENTER_Y           , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::CENTER_Z           , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::CENTER_LAMBDA      , 1./Gaudi::Units::millimeter  },
  { xAOD::CaloCluster::LATERAL            , 1.0                          },
  { xAOD::CaloCluster::LONGITUDINAL       , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_EM        , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_MAX       , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_CORE      , 1.0                          },
  { xAOD::CaloCluster::ENG_FRAC_TIME      , 1.0                          },
  { xAOD::CaloCluster::CELL_TIME_MAX      , 1./Gaudi::Units::nanosecond  },
  { xAOD::CaloCluster::FIRST_ENG_DENS     , 1./(Gaudi::Units::GeV/Gaudi::Units::millimeter3)                                                 },
  { xAOD::CaloCluster::SECOND_ENG_DENS    , 1./((Gaudi::Units::GeV/Gaudi::Units::millimeter3)*(Gaudi::Units::GeV/Gaudi::Units::millimeter3)) },
  { xAOD::CaloCluster::ISOLATION          , 1.0                          },
  { xAOD::CaloCluster::ENG_BAD_CELLS      , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::N_BAD_CELLS        , 1.0                          },
  { xAOD::CaloCluster::N_BAD_CELLS_CORR   , 1.0                          },
  { xAOD::CaloCluster::BAD_CELLS_CORR_E   , 1.0                          },
  { xAOD::CaloCluster::BADLARQ_FRAC       , 1.0                          },
  { xAOD::CaloCluster::ENG_POS            , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::SIGNIFICANCE       , 1.0                          },
  { xAOD::CaloCluster::CELL_SIGNIFICANCE  , 1.0                          },
  { xAOD::CaloCluster::CELL_SIG_SAMPLING  , 1.0                          },
  { xAOD::CaloCluster::AVG_LAR_Q          , 1.0                          },
  { xAOD::CaloCluster::AVG_TILE_Q         , 1.0                          },
  { xAOD::CaloCluster::ENG_BAD_HV_CELLS   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::N_BAD_HV_CELLS     , 1.0                          },
  { xAOD::CaloCluster::PTD                , 1.0                          },
  { xAOD::CaloCluster::MASS               , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::EM_PROBABILITY     , 1.0                          },
  { xAOD::CaloCluster::ENG_CALIB_TOT      , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_OUT_T    , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_OUT_L    , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_OUT_M    , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_T   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_L   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_M   , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_DEAD_TOT , 1./Gaudi::Units::GeV         },
  { xAOD::CaloCluster::ENG_CALIB_FRAC_EM  , 1.0                          },
  { xAOD::CaloCluster::ENG_CALIB_FRAC_HAD , 1.0                          },
  { xAOD::CaloCluster::ENG_CALIB_FRAC_REST, 1.0                          },
  { xAOD::CaloCluster::SECOND_TIME        , 1./(Gaudi::Units::nanosecond*Gaudi::Units::nanosecond) }
};

namespace {
  // MsgStream& operator<<(MsgStream& mstr,const SG::ReadHandleKey<xAOD::TruthParticleContainer>& ckey) { mstr << ckey.key(); return mstr; }
  // MsgStream& operator<<(MsgStream& mstr,const SG::ReadHandleKey<xAOD::CaloClusterContainer>&   ckey) { mstr << ckey.key(); return mstr; }
  // MsgStream& operator<<(MsgStream& mstr,const SG::ReadHandleKey<xAOD::EventInfo>&              ckey) { mstr << ckey.key(); return mstr; }
  template<class HANDLEKEY> std::string handleKey(const HANDLEKEY& hkey ) { return hkey.key(); }
}

/////////////////
// Constructor //
/////////////////

ClusterTreeMaker::ClusterTreeMaker( const std::string& name, ISvcLocator* pSvcLocator ) 
  : AthHistogramAlgorithm( name, pSvcLocator )
{
  // -- data 
  declareProperty("Prefix",                m_prefix                   );
  declareProperty("CaloClusterContainer",  m_clusterContainerKey      );
  declareProperty("TruthParticleContainer",m_truthParticleContainerKey);  
  declareProperty("JetContainer",          m_jetContainerKey          );          
  declareProperty("TruthJetContainer",     m_truthJetContainerKey     );
  declareProperty("EventInfo",             m_eventInfoKey             );
					   
  // -- topo-cluster phasespace			   
  declareProperty("ClusterEmin",           m_clusterEmin             ,"minimum topo-cluster signal at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale"   );
  declareProperty("ClusterEmax",           m_clusterEmax             ,"maximum topo-cluster signal at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale"   );
  declareProperty("ClusterRapMin",         m_clusterRapMin           ,"minimum topo-cluster rapidity at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale ");
  declareProperty("ClusterRapMax",         m_clusterRapMax           ,"maximum topo-cluster rapidity at LCW (UseLCWScaleAsReference=true) or EM (UseLCWScaleAsReference=false) scale ");

  // -- topo-cluster truth phase space
  declareProperty("ClusterTruthEmin",      m_clusterTruthEmin        ,"minimum true (deposited) energy in topo-cluster");
  declareProperty("ClusterTruthEmax",      m_clusterTruthEmax        ,"maximum true (deposited) energy in topo-cluster");

  // -- jet phase space
  declareProperty("JetPtMin",              m_jetPtMin                ,"minimum jet pT at JES scale"      );
  declareProperty("JetRapMin",             m_jetRapMin               ,"minimum jet rapidity at JES scale");
  declareProperty("JetRapMax",             m_jetRapMax               ,"maximum jet rapidity at JES scale");
  declareProperty("JetMatchingRadius",     m_jetMatchRadius          ,"truth-reco jet matching radius"   );

  // -- tuple configuration
  declareProperty("CaloClusterMomentNames",m_caloClusterMoments      ,"list of topo-cluster moment names to be written out"                     );

  // -- process control
  declareProperty("UncalibratedClusters"  ,m_doUnCalibratedClusters  ,"include uncalibrated (EM) scale clusters"                                );
  declareProperty("UseSignalStates"       ,m_useSignalStates         ,"use signal states for EM/LCW scale, instead of sister cluster allocation");
  declareProperty("UseLCWScaleAsReference",m_useLCWScaleAsReference  ,"use LCW kinematics for cluster selection, instead of EM"                 );
  declareProperty("UseSingleParticles"    ,m_useSingleParticles      ,"analyse single particles"                                                );
  declareProperty("UseJets"               ,m_useJets                 ,"analyse jets"                                                            );
  declareProperty("MatchTruthJets"        ,m_matchTruthJets          ,"match reco jets with truth jets if jets are analyzed"                    );
  declareProperty("LCWConstituents"       ,m_calibratedConstituents  ,"jet constituents are at LCW scale"                                       );
  declareProperty("DebugStreamFlag"       ,m_debugStreamFlag         ,"write additional debugging information to file"                          );
  declareProperty("DebugStreamFile"       ,m_debugStreamName         ,"name of file associated with debug stream"                               );
}

ClusterTreeMaker::~ClusterTreeMaker() {}

////////////////////
// Initialization //
////////////////////

StatusCode ClusterTreeMaker::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");

  // -- need a prefix (?)
  if (m_prefix=="") { ATH_MSG_WARNING("No decoration prefix name provided"); }

  // -- signal state for constituents
  m_calibratedConstituents |= handleKey(m_jetContainerKey).find("LC") != std::string::npos; 
  m_constituentScale       =  m_calibratedConstituents ? xAOD::CaloCluster::CALIBRATED : xAOD::CaloCluster::UNCALIBRATED; 

  // -- report configuration
  char buffer0[128];
  sprintf(buffer0,"Property: Prefix ...................... \042%s\042"    ,m_prefix.c_str()                                                        ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: CaloClusterContainer ........ \042%s\042"    ,handleKey(m_clusterContainerKey).c_str()                                ); ATH_MSG_INFO( buffer0 );
  if ( m_useSingleParticles) { sprintf(buffer0,"Property: TruthParticleContainer ...... \042%s\042",handleKey(m_truthParticleContainerKey).c_str()); ATH_MSG_INFO( buffer0 ); }
  if ( m_useJets ) {   
    sprintf(buffer0,"Property: LCW-scale jet constituents .. %s"        ,boolToChar.at(m_calibratedConstituents)                                                                    ); ATH_MSG_INFO( buffer0 ); 
    sprintf(buffer0,"Property: JetContainer ................ \042%s\042 (constituents at %s scale)",handleKey(m_jetContainerKey).c_str(),enumToScale.at(m_constituentScale).c_str() ); ATH_MSG_INFO( buffer0 ); 
    sprintf(buffer0,"Property: VertexContainer ............. \042%s\042",handleKey(m_vertexContainerKey).c_str()                                                                    ); ATH_MSG_INFO( buffer0 );
    if ( m_matchTruthJets ) { sprintf(buffer0,"Property: TruthJetContainerKey ........ \042%s\042",handleKey(m_truthJetContainerKey).c_str()); ATH_MSG_INFO( buffer0 ); }
  }
  // ----- exception only for truth to avoid scale factor application for each cluster!
  // m_clusterTruthEmin *= m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_TOT); 
  // m_clusterTruthEmax *= m_knownClusterMomentScale.at(xAOD::CaloCluster::ENG_CALIB_TOT); 
  sprintf(buffer0,"Property: EventInfo ................... \042%s\042"    ,handleKey(m_eventInfoKey).c_str()                                       ); ATH_MSG_INFO( buffer0 );  
  sprintf(buffer0,"Property: ClusterEmin ............[GeV] %.3f"          ,m_clusterEmin/Gaudi::Units::GeV                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterEmax ............[GeV] %.3f"          ,m_clusterEmax/Gaudi::Units::GeV                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterTruthEmin .......[GeV] %.3f"          ,m_clusterTruthEmin/Gaudi::Units::GeV                                    ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterTruthEmax .......[GeV] %.3f"          ,m_clusterTruthEmax/Gaudi::Units::GeV                                    ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterRapMin ............... %.1f"          ,m_clusterRapMin                                                         ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: ClusterRapMax ............... %.1f"          ,m_clusterRapMax                                                         ); ATH_MSG_INFO( buffer0 );
  if ( m_useJets ) { 
    sprintf(buffer0,"Property: JetPtMin ...............[GeV] %.3f"          ,m_jetPtMin/Gaudi::Units::GeV                                            ); ATH_MSG_INFO( buffer0 );
    sprintf(buffer0,"Property: JetRapMin ................... %.1f"          ,m_jetRapMin                                                             ); ATH_MSG_INFO( buffer0 );
    sprintf(buffer0,"Property: JetRapMax ................... %.1f"          ,m_jetRapMax                                                             ); ATH_MSG_INFO( buffer0 );
  }
  sprintf(buffer0,"Property: CaloClusterMomentNames ...... %s - %zu names",boolToChar.at(!m_caloClusterMoments.empty()),m_caloClusterMoments.size()); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UncalibratedClusters ........ %s"            ,boolToChar.at(m_doUnCalibratedClusters)                                 ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseLCWasReference ........... %s"            ,boolToChar.at(m_useLCWScaleAsReference)                                 ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseSignalStates ......... ... %s"            ,boolToChar.at(m_useSignalStates)                                        ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: UseSingleParticles .......... %s"            ,boolToChar.at(m_useSingleParticles)                                     ); ATH_MSG_INFO( buffer0 );  
  sprintf(buffer0,"Property: UseJets ..................... %s"            ,boolToChar.at(m_useJets)                                                ); ATH_MSG_INFO( buffer0 );  
  if ( m_useJets ) { 
    sprintf(buffer0,"Property: MatchTruthJets .............. %s"            ,boolToChar.at(m_matchTruthJets)                                         ); ATH_MSG_INFO( buffer0 );  
    sprintf(buffer0,"Property: JetMatchingRadius ........... %.2f"          ,m_jetMatchRadius                                                        ); ATH_MSG_INFO( buffer0 );  
  }
  sprintf(buffer0,"Property: Debug stream activated ...... %s"            ,boolToChar.at(m_debugStreamFlag)                                        ); ATH_MSG_INFO( buffer0 );
  sprintf(buffer0,"Property: Debug stream filename ....... %s"            ,m_debugStreamName.c_str()                                               ); ATH_MSG_INFO( buffer0 );

  // -- inconsistency
  if ( m_doUnCalibratedClusters && m_useSignalStates ) {
    ATH_MSG_ERROR( "invalid configuration - UncalibratedClusters == true retrieves EM-scale kinematics from sister clusters while UseSignalStates == true uses signal states of the calibrated cluster - select only one option!" );
    return StatusCode::FAILURE;
  }

  // -- check read handle keys 
  ATH_CHECK(m_clusterContainerKey.initialize());
  ATH_CHECK(m_eventInfoKey.initialize()); 
  ATH_CHECK(m_vertexContainerKey.initialize());
  if ( m_useSingleParticles ) { ATH_CHECK(m_truthParticleContainerKey.initialize()); }
  if ( m_useJets            ) { ATH_CHECK(m_jetContainerKey.initialize())          ; }
  if ( m_matchTruthJets     ) { ATH_CHECK(m_truthJetContainerKey.initialize())     ; }

  // -- book cluster TTree and its branches
  CHECK( book(TTree("ClusterTree", "ClusterTree")) );
  m_clusterTree = tree("ClusterTree");

  // -- event info 
  m_clusterTree->Branch("seqNumber",  &m_sequence,   "seqNumber/L"  );
  m_clusterTree->Branch("runNumber",  &m_runNumber,  "runNumber/I"  );
  m_clusterTree->Branch("eventNumber",&m_eventNumber,"eventNumber/L");

  // -- pile-up (fixed for single particles)
  m_clusterTree->Branch("avgMu"    ,&m_averageMu  ,"avgMu/F"    );
  m_clusterTree->Branch("nPrimVtx" ,&m_nPrimVtx   ,"nPrimVtx/I" ); 

  // -- truth particle kinematics and id
  if ( m_useSingleParticles ) {
    m_clusterTree->Branch("truthCnt",&m_fParticleCount  ,"truthCnt/I");
    m_clusterTree->Branch("truthE"  ,&m_fClusterTruthE  ,"truthE/F"  );
    m_clusterTree->Branch("truthPt" ,&m_fClusterTruthPt ,"truthPt/F" );
    m_clusterTree->Branch("truthEta",&m_fClusterTruthEta,"truthEta/F");
    m_clusterTree->Branch("truthPhi",&m_fClusterTruthPhi,"truthPhi/F");
    m_clusterTree->Branch("truthPDG",&m_fClusterTruthPDG,"truthPDG/I");
  }

  // -- jet kinematics and composition
  if ( m_useJets ) { 
    m_clusterTree->Branch("jetCnt"   ,&m_fJetCount  ,"jetCnt/L"   );
    m_clusterTree->Branch("jetCalE"  ,&m_fJetCalE   ,"jetCalE/F"  );
    m_clusterTree->Branch("jetCalPt" ,&m_fJetCalPt  ,"jetCalPt/F" );
    m_clusterTree->Branch("jetCalEta",&m_fJetCalRap ,"jetCalEta/F");
    m_clusterTree->Branch("jetCalPhi",&m_fJetCalPhi ,"jetCalPhi/F");
    m_clusterTree->Branch("jetRawE"  ,&m_fJetRawE   ,"jetRawE/F"  );
    m_clusterTree->Branch("jetRawPt" ,&m_fJetRawPt  ,"jetRawPt/F" );
    m_clusterTree->Branch("jetRawEta",&m_fJetRawRap ,"jetRawEta/F");
    m_clusterTree->Branch("jetRawPhi",&m_fJetRawPhi ,"jetRawPhi/F");
    m_clusterTree->Branch("jetNConst",&m_fJetNConst ,"jetNConst/I");
    if ( m_matchTruthJets ) { 
      m_clusterTree->Branch("truthJetMatchRadius",&m_fTruthJetMatchRadius,"truthJetMatchRadius/F");
      m_clusterTree->Branch("truthJetE"          ,&m_fTruthJetE          ,"truthJetE/F"          );
      m_clusterTree->Branch("truthJetPt"         ,&m_fTruthJetPt         ,"truthJetPt/F"         );
      m_clusterTree->Branch("truthJetRap"        ,&m_fTruthJetRap        ,"truthJetRap/F"        );
      m_clusterTree->Branch("truthJetPhi"        ,&m_fTruthJetPhi        ,"truthJetPhi/F"        );
    }
  }

  // -- cluster indices etc.
  m_clusterTree->Branch("nCluster"          ,&m_nCluster           ,"nCluster/I"          );
  m_clusterTree->Branch("clusterIndex"      ,&m_fClusterIndex      ,"clusterIndex/I"      );
  m_clusterTree->Branch("cluster_nCells"    ,&m_fCluster_nCells    ,"cluster_nCells/I"    );
  m_clusterTree->Branch("cluster_nCells_tot",&m_fCluster_nCells_tot,"cluster_nCells_tot/I");

  // -- cluster kinematics: LCW scale
  m_clusterTree->Branch("clusterECalib"         ,&m_fClusterECalib         ,"clusterECalib/F"          );
  m_clusterTree->Branch("clusterPtCalib"        ,&m_fClusterPtCalib        ,"clusterPtCalib/F"         );    
  m_clusterTree->Branch("clusterEtaCalib"       ,&m_fClusterEtaCalib       ,"clusterEtaCalib/F"        );    
  m_clusterTree->Branch("clusterPhiCalib"       ,&m_fClusterPhiCalib       ,"clusterPhiCalib/F"        );    
  m_clusterTree->Branch("cluster_sumCellECalib" ,&m_fCluster_sumCellECalib ,"cluster_sumCellECAlib/F"  );
  m_clusterTree->Branch("cluster_fracECalib"    ,&m_fCluster_fracECalib    ,"cluster_fracECalib/F"     );
  m_clusterTree->Branch("cluster_fracECalib_ref",&m_fCluster_fracECalib_ref,"cluster_fracECalib_ref/F" );

  // -- cluster kinematics: EM scale if doUnCalibratedClusters == true (default)
  if ( m_doUnCalibratedClusters || m_useSignalStates ) { 
    m_clusterTree->Branch("clusterE"         ,&m_fClusterE         ,"clusterE/F"          );    
    m_clusterTree->Branch("clusterPt"        ,&m_fClusterPt        ,"clusterPt/F"         );    
    m_clusterTree->Branch("clusterEta"       ,&m_fClusterEta       ,"clusterEta/F"        );    
    m_clusterTree->Branch("clusterPhi"       ,&m_fClusterPhi       ,"clusterPhi/F"        );    
    m_clusterTree->Branch("cluster_sumCellE" ,&m_fCluster_sumCellE ,"cluster_sumCellE/F"  );
    m_clusterTree->Branch("cluster_time"     ,&m_fClusterTime      ,"cluster_time/F"      );
    m_clusterTree->Branch("cluster_fracE"    ,&m_fCluster_fracE    ,"cluster_fracE/F"     );
    m_clusterTree->Branch("cluster_fracE_ref",&m_fCluster_fracE_ref,"cluster_fracE_ref/F" );
  }

  // -- cluster calibration: weights and true energies
  m_clusterTree->Branch("cluster_EM_PROBABILITY",     &m_fCluster_EM_PROBABILITY,     "cluster_EM_PROBABILITY/F"     ); // calibrated cluster!!
  m_clusterTree->Branch("cluster_HAD_WEIGHT",         &m_fCluster_HAD_WEIGHT,         "cluster_HAD_WEIGHT/F"         );
  m_clusterTree->Branch("cluster_OOC_WEIGHT",         &m_fCluster_OOC_WEIGHT,         "cluster_OOC_WEIGHT/F"         );
  m_clusterTree->Branch("cluster_DM_WEIGHT",          &m_fCluster_DM_WEIGHT,          "cluster_DM_WEIGHT/F"          );
  m_clusterTree->Branch("cluster_ENG_CALIB_TOT",      &m_fCluster_ENG_CALIB_TOT,      "cluster_ENG_CALIB_TOT/F"      );  
  m_clusterTree->Branch("cluster_ENG_CALIB_OUT_T",    &m_fCluster_ENG_CALIB_OUT_T,    "cluster_ENG_CALIB_OUT_T/F"    );
  m_clusterTree->Branch("cluster_ENG_CALIB_OUT_L",    &m_fCluster_ENG_CALIB_OUT_L,    "cluster_ENG_CALIB_OUT_L/F"    );
  m_clusterTree->Branch("cluster_ENG_CALIB_OUT_M",    &m_fCluster_ENG_CALIB_OUT_M,    "cluster_ENG_CALIB_OUT_M/F"    );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_T",   &m_fCluster_ENG_CALIB_DEAD_T,   "cluster_ENG_CALIB_DEAD_T/F"   );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_L",   &m_fCluster_ENG_CALIB_DEAD_L,   "cluster_ENG_CALIB_DEAD_L/F"   );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_M",   &m_fCluster_ENG_CALIB_DEAD_M,   "cluster_ENG_CALIB_DEAD_M/F"   );
  m_clusterTree->Branch("cluster_ENG_CALIB_DEAD_TOT", &m_fCluster_ENG_CALIB_DEAD_TOT, "cluster_ENG_CALIB_DEAD_TOT/F" );
  m_clusterTree->Branch("cluster_ENG_CALIB_FRAC_EM" , &m_fCluster_ENG_CALIB_FRAC_EM,  "cluster_ENG_CALIB_FRAC_EM/F"  );
  m_clusterTree->Branch("cluster_ENG_CALIB_FRAC_HAD" ,&m_fCluster_ENG_CALIB_FRAC_HAD, "cluster_ENG_CALIB_FRAC_HAD/F" );
  m_clusterTree->Branch("cluster_ENG_CALIB_FRAC_REST",&m_fCluster_ENG_CALIB_FRAC_REST,"cluster_ENG_CALIB_FRAC_REST/F"); 

  // -- cluster moments (defaults from previous studies 
  m_clusterTree->Branch("cluster_CENTER_MAG",        &m_fCluster_CENTER_MAG,        "cluster_CENTER_MAG/F"        );
  m_clusterTree->Branch("cluster_FIRST_ENG_DENS",    &m_fCluster_FIRST_ENG_DENS,    "cluster_FIRST_ENG_DENS/F"    );

  // -- cluster moments (dynamically configured)
  std::vector<std::string> defClusterMomentNames = { "CENTER_MAG", "EM_PROBABILITY", "FIRST_ENG_DENS" };
  std::vector<std::string> actClusterMomentNames;
  for ( const auto& mname : m_caloClusterMoments ) {
    if ( (m_knownClusterMomentType.find(mname) != m_knownClusterMomentType.end()) && 
	 (std::find(defClusterMomentNames.begin(),defClusterMomentNames.end(),mname) == defClusterMomentNames.end()) ) { actClusterMomentNames.push_back(mname); }
  }

  // -- cluster moment configuration report
  ATH_MSG_INFO( "Number of moments requested " << actClusterMomentNames.size() );
  std::string msg; char buffer1[128];
  size_t nwrds(0);
  for ( auto fiter(m_knownClusterMomentName.begin()); fiter != m_knownClusterMomentName.end(); ++fiter ) { 
    nwrds = std::max(fiter->second.length(),nwrds);
  }
  int ictr(0);
  for ( const auto& mom : actClusterMomentNames ) {
    if ( ictr == 4 ) {  ATH_MSG_INFO( "....." << msg ); ictr = 0; msg = ""; }
    sprintf(buffer1," %-*.*s",(int)nwrds,(int)nwrds,mom.c_str());
    msg += std::string(buffer1);
    ++ictr;
  } 
  if ( msg != "" ) { ATH_MSG_INFO( "....." << msg ); } 

  // -- cluster moments - set up dynamic moment cache 
  float* fpntr = (float*)0; // dummy load
  for ( const auto& mname : actClusterMomentNames ) { 
    xAOD::CaloCluster::MomentType cmtype  = m_knownClusterMomentType.at(mname);
    double                        cmscale = m_knownClusterMomentScale.at(cmtype);
    m_caloClusterMomentLink.push_back(std::make_tuple(cmtype,mname,fpntr,cmscale));
  }

  // -- cluster moments - book branches
  if ( !this->bookClusterMoments(nwrds) ) { ATH_MSG_WARNING("no cluster moments booked in tuple!"); } 

  // -- open debug stream file
  if ( m_debugStreamFlag ) {
    if ( m_debugStream.is_open() ) { m_debugStream.close(); }
    if ( m_debugStreamName == "" ) { ATH_MSG_ERROR( "debug stream is requested (flag = " << boolToName.at(m_debugStreamFlag) << ") but the stream name is empty/invalid"); return StatusCode::FAILURE; }  
    m_debugStream.open(m_debugStreamName); 
    if ( !m_debugStream.good() ) { 
      ATH_MSG_WARNING( "debug stream is requested (flag = " << boolToChar.at(m_debugStreamFlag) << ") but is in bad state: turning it off!"); m_debugStreamFlag = false;  
    } else {
      ATH_MSG_INFO   ( "debug stream is requested (flag = " << boolToChar.at(m_debugStreamFlag) << ") and sucessfully opened"              );
    }
  }

  return StatusCode::SUCCESS;

} // initialize()

/////////////
// Execute //
/////////////

StatusCode ClusterTreeMaker::execute() {  

  char buffer[1024];
  static const std::string mname = "ClusterTreeMaker::execute()";

  // -- overall processing flag
  StatusCode checkProc(StatusCode::SUCCESS);

  // -- reset all variables
  this->resetAll();

  // -- principal event counter
  ++m_allEvntCtr;
  sprintf(buffer,"%s@BeginEvent",codeToName.at(checkProc).c_str());
  resourceMessage(mname,std::string(buffer));

  // -- general event information
  SG::ReadHandle<xAOD::EventInfo> eventInfoHandle(m_eventInfoKey);
  if ( !eventInfoHandle.isValid()) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::EventInfo object with key <" << eventInfoHandle.key() << ">" ); return StatusCode::FAILURE; }
  debugMessage(mname,"#--- created handle \042%s\042",eventInfoHandle.key().c_str());

  // -- calo cluster container
  SG::ReadHandle<xAOD::CaloClusterContainer> clusterContainerHandle(m_clusterContainerKey); 
  if ( !clusterContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::CaloClusterContainer object with key <" << clusterContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
  debugMessage(mname,"#--- created handle \042%s\042",clusterContainerHandle.key().c_str());

  // -- reset variables
  // -- collect some event information
  m_runNumber   = eventInfoHandle->runNumber();
  m_eventNumber = eventInfoHandle->eventNumber();
  debugMessage(mname,"# --- run number %08i event number %08lli",m_runNumber,m_eventNumber);
  //  if ( m_useJets ) { 
  m_averageMu = eventInfoHandle->actualInteractionsPerCrossing(); 
  SG::ReadHandle<xAOD::VertexContainer> vertexContainerHandle(m_vertexContainerKey);
  if ( !vertexContainerHandle.isValid() ) { 
    ATH_MSG_ERROR( "cannot retrieve vertex container with key <" << vertexContainerHandle.key() << ">" ); 
    if ( m_useJets ) { return StatusCode::FAILURE; } else { m_nPrimVtx = 1; } 
  } else { 
    for ( auto pVtx : *(vertexContainerHandle.cptr()) ) { if ( pVtx->vertexType() == xAOD::VxType::PriVtx || pVtx->vertexType() == xAOD::VxType::PileUp ) { ++m_nPrimVtx; } }
    debugMessage(mname,"# ----- [jets/particles] number of primary vertices %3i <mu> = %4.1f",m_nPrimVtx,m_averageMu);
  }

  // -- truth particles
  if ( m_useSingleParticles ) { 
    SG::ReadHandle<xAOD::TruthParticleContainer> truthParticleContainerHandle(m_truthParticleContainerKey);
    if ( !truthParticleContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::TruthParticleContainer object with key <" << truthParticleContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
    // m_averageMu = 0.;
    // m_nPrimVtx  = 1; 
    if( truthParticleContainerHandle->empty() ) { 
      m_fClusterTruthE   = -1.;
      m_fClusterTruthPDG = 0;
    } else {
      m_fClusterTruthE   = truthParticleContainerHandle->at(0)->e() /Gaudi::Units::GeV;
      m_fClusterTruthPt  = truthParticleContainerHandle->at(0)->pt()/Gaudi::Units::GeV;
      m_fClusterTruthEta = truthParticleContainerHandle->at(0)->eta();
      m_fClusterTruthPhi = truthParticleContainerHandle->at(0)->phi();
      m_fClusterTruthPDG = truthParticleContainerHandle->at(0)->pdgId();
      // get all clusters for particle
      ++m_fParticleCount; 
      checkProc = fillClusters<xAOD::CaloClusterContainer>(clusterContainerHandle.cptr(),m_fClusterTruthE) ? StatusCode::SUCCESS : StatusCode::FAILURE;
    } // valid truth information
  } // truth particle (single particle) mode required

  // -- jets 
  if ( m_useJets ) { 
    SG::ReadHandle<xAOD::JetContainer> jetContainerHandle(m_jetContainerKey); 
    if ( !jetContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::JetContainer object with key <" << jetContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
    if ( jetContainerHandle->empty() )   { ATH_MSG_WARNING( "no reco jets in container <" << jetContainerHandle.key() << ">"                                  ); return StatusCode::SUCCESS; } 
    debugMessage(mname,"# ----- [jets] reco jet container .... <%s>",jetContainerHandle.key().c_str());
    debugMessage(mname,"# ----- [jets] # reco jets ........... %zu" ,jetContainerHandle->size()      );
    SG::ReadHandle<xAOD::JetContainer> truthJetContainerHandle(m_truthJetContainerKey); 
    if ( m_matchTruthJets && !truthJetContainerHandle.isValid() ) { ATH_MSG_ERROR( "cannot retrieve handle to xAOD::JetContainer object with key <" << truthJetContainerHandle.key() << ">" ); return StatusCode::FAILURE; }
    debugMessage(mname,"# ----- [jets] truth jet container ... <%s>",truthJetContainerHandle.key().c_str());
    debugMessage(mname,"# ----- [jets] # truth jets .......... %zu" ,truthJetContainerHandle->size()      );
    // check on truth jet matching
    if ( m_matchTruthJets ) { 
      debugMessage(mname,"# ----- [jets] --> jet matching - enter ClusterTreeMaker::findMatches(...)");
      // get matching reco-truth jets (also applies filter on reco jets)
      MatchedJetPairVector matches(this->findMatches(*jetContainerHandle,*truthJetContainerHandle)); 
      sprintf(buffer,"# ----- [jets] --> jet matching - invocation for %zu truth and %zu reco jets",truthJetContainerHandle->size(),jetContainerHandle->size());
      if ( !matches.empty() ) { 
	auto fmatch(matches.begin());
	debugMessage(mname,"# ----- [jets] --> jet matching - found %zu matches",matches.size());
	while ( fmatch != matches.end() && checkProc.isSuccess() ) {
	  checkProc = this->fillTruthJetBlock(std::get<1>(*fmatch),std::get<2>(*fmatch)) ? StatusCode::SUCCESS : StatusCode::FAILURE;
	  if ( checkProc.isSuccess() ) { 
	    debugMessage(mname,"# ----- [jets] --> jet matching - matching succeeded, truth jet block fill: %s [match: %2zu]",codeToName.at(checkProc).c_str(),(size_t)(fmatch-matches.begin()));
	    checkProc = this->fillRecoJetBlock(std::get<0>(*fmatch),m_constituentScale) ? StatusCode::SUCCESS : StatusCode::FAILURE; 
	    debugMessage(mname,"# ----- [jets] --> jet matching - matching succeeded, reco  jet block fill: %s [match: %2zu]",codeToName.at(checkProc).c_str(),(size_t)(fmatch-matches.begin()));
	  } else {
	    debugMessage(mname,"# ----- [jets] --> jet matching - matching succeeded, truth jet block fill: %s [match: %2zu]",codeToName.at(checkProc).c_str(),(size_t)(fmatch-matches.begin()));
	  }
	  ++fmatch;
	} // loop matched
      } else {                                                                                   					                    
	debugMessage(mname,"# ----- [jets] --> jet matching - no matches found");                                                            
      }
    } else {                                                                                
      // record only reco jets
      auto fjet(jetContainerHandle->begin()); 
      while ( fjet != jetContainerHandle->end() && checkProc.isSuccess() ) { if ( this->applyFilter(**fjet) ) { checkProc = this->fillRecoJetBlock(*fjet,m_constituentScale) ? StatusCode::SUCCESS : StatusCode::FAILURE; } ++fjet; }
    } // check on matching requirement
    if ( m_allEvntCtr <= 10 || m_allEvntCtr % 100 == 0 ) { 
      sprintf(buffer,"Run %8i Event %10lli <mu> = %5.1f Npv = %3i number of jets %zu",m_runNumber,m_eventNumber,m_averageMu,m_nPrimVtx,jetContainerHandle->size()); ATH_MSG_INFO( buffer ); 
    }
  } // jet use required
  if ( checkProc.isFailure() ) { checkProc = StatusCode::RECOVERABLE; }
  sprintf(buffer,"%s@EndEvent  ",codeToName.at(checkProc).c_str());
  resourceMessage(mname,buffer); 
  // return StatusCode::SUCCESS;
  return checkProc;
} // execute()

StatusCode ClusterTreeMaker::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  
  if ( m_allEvntCtr > 0 ) {
    // find number of digits (up to 12)
    int nmax = std::min(std::max( { m_allEvntCtr, m_allClusCtr, 1 } ),12);
    int ndig = std::min(std::max(((int)std::log10((double)nmax))+3,1),12);
    // format output
    char buffer[2048];
    sprintf(buffer,"total # events .....: %*i accepted # events .....: %*i -> %6.2f%%",ndig,m_allEvntCtr,ndig,m_accEvntCtr,((double)m_accEvntCtr)/((double)m_allEvntCtr)*100.);
    ATH_MSG_INFO( buffer );
    // all clusters
    if ( m_allClusCtr > 0 ) { 
      sprintf(buffer,"total # clusters ...: %*i accepted # clusters ...: %*i -> %6.2f%%",ndig,m_allClusCtr,ndig,m_accClusCtr,((double)m_accClusCtr)/((double)m_allClusCtr)*100.);
    } else { 
      sprintf(buffer,"total # clusters ...: %*i accepted # clusters ...: %*i",ndig,m_allClusCtr,ndig,m_accClusCtr);
    }
    ATH_MSG_INFO( buffer );
    // per accepted event stats
    sprintf(buffer,"<#clusters/event> ..: %*.3f (accepted events only)",ndig,((double)m_accClusCtr)/((double)m_accEvntCtr));
    ATH_MSG_INFO( buffer ); 
  }

  return StatusCode::SUCCESS;
}

void ClusterTreeMaker::resetClusterMomentLinkValue(float value) { 
  for ( auto& mlnk : m_caloClusterMomentLink ) {
    if ( std::get<2>(mlnk) != 0 ) { *(std::get<2>(mlnk)) = value; } 
  }
}

void ClusterTreeMaker::registerClusterMoment(MomentLink& mlink) { 
  static std::string _prefix = "cluster_";
  static std::string _vtype  = "/F";
  std::string bname(_prefix+std::get<1>(mlink));
  float*      baddr(std::get<2>(mlink)); 
  std::string bdesc(bname+_vtype);
  m_clusterTree->Branch(bname.c_str(),baddr,bdesc.c_str());
}

bool ClusterTreeMaker::bookClusterMoments(size_t nwrds) {
  char buffer[256];
  // book branches for requested moments
  int lwrds((int)nwrds);
  for ( auto& mlnk : m_caloClusterMomentLink ) { 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_MAG       ) { std::get<2>(mlnk) = &m_fCluster_CENTER_MAG       ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::FIRST_PHI        ) { std::get<2>(mlnk) = &m_fCluster_FIRST_PHI        ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::FIRST_ETA        ) { std::get<2>(mlnk) = &m_fCluster_FIRST_ETA        ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_R         ) { std::get<2>(mlnk) = &m_fCluster_SECOND_R         ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_LAMBDA    ) { std::get<2>(mlnk) = &m_fCluster_SECOND_LAMBDA    ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::DELTA_PHI        ) { std::get<2>(mlnk) = &m_fCluster_DELTA_PHI        ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::DELTA_THETA      ) { std::get<2>(mlnk) = &m_fCluster_DELTA_THETA      ; this->registerClusterMoment(mlnk); } 
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::DELTA_ALPHA      ) { std::get<2>(mlnk) = &m_fCluster_DELTA_ALPHA      ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_X         ) { std::get<2>(mlnk) = &m_fCluster_CENTER_X         ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_Y         ) { std::get<2>(mlnk) = &m_fCluster_CENTER_Y         ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_Z         ) { std::get<2>(mlnk) = &m_fCluster_CENTER_Z         ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CENTER_LAMBDA    ) { std::get<2>(mlnk) = &m_fCluster_CENTER_LAMBDA    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::LATERAL          ) { std::get<2>(mlnk) = &m_fCluster_LATERAL          ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::LONGITUDINAL     ) { std::get<2>(mlnk) = &m_fCluster_LONGITUDINAL     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_EM      ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_EM      ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_MAX     ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_MAX     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_CORE    ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_CORE    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_FRAC_TIME    ) { std::get<2>(mlnk) = &m_fCluster_ENG_FRAC_TIME    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CELL_TIME_MAX    ) { std::get<2>(mlnk) = &m_fCluster_CELL_TIME_MAX    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::FIRST_ENG_DENS   ) { std::get<2>(mlnk) = &m_fCluster_FIRST_ENG_DENS   ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_ENG_DENS  ) { std::get<2>(mlnk) = &m_fCluster_SECOND_ENG_DENS  ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ISOLATION        ) { std::get<2>(mlnk) = &m_fCluster_ISOLATION        ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_BAD_CELLS    ) { std::get<2>(mlnk) = &m_fCluster_ENG_BAD_CELLS    ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::N_BAD_CELLS      ) { std::get<2>(mlnk) = &m_fCluster_N_BAD_CELLS      ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::N_BAD_CELLS_CORR ) { std::get<2>(mlnk) = &m_fCluster_N_BAD_CELLS_CORR ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::BAD_CELLS_CORR_E ) { std::get<2>(mlnk) = &m_fCluster_BAD_CELLS_CORR_E ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::BADLARQ_FRAC     ) { std::get<2>(mlnk) = &m_fCluster_BADLARQ_FRAC     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_POS          ) { std::get<2>(mlnk) = &m_fCluster_ENG_POS          ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SIGNIFICANCE     ) { std::get<2>(mlnk) = &m_fCluster_SIGNIFICANCE     ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CELL_SIGNIFICANCE) { std::get<2>(mlnk) = &m_fCluster_CELL_SIGNIFICANCE; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::CELL_SIG_SAMPLING) { std::get<2>(mlnk) = &m_fCluster_CELL_SIG_SAMPLING; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::AVG_LAR_Q        ) { std::get<2>(mlnk) = &m_fCluster_AVG_LAR_Q        ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::AVG_TILE_Q       ) { std::get<2>(mlnk) = &m_fCluster_AVG_TILE_Q       ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::ENG_BAD_HV_CELLS ) { std::get<2>(mlnk) = &m_fCluster_ENG_BAD_HV_CELLS ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::N_BAD_HV_CELLS   ) { std::get<2>(mlnk) = &m_fCluster_N_BAD_HV_CELLS   ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::PTD              ) { std::get<2>(mlnk) = &m_fCluster_PTD              ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::MASS             ) { std::get<2>(mlnk) = &m_fCluster_MASS             ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::EM_PROBABILITY   ) { std::get<2>(mlnk) = &m_fCluster_EM_PROBABILITY   ; this->registerClusterMoment(mlnk); }
    if ( std::get<0>(mlnk) == xAOD::CaloCluster::SECOND_TIME      ) { std::get<2>(mlnk) = &m_fCluster_SECOND_TIME      ; this->registerClusterMoment(mlnk); }
    // print
    sprintf(buffer,"moment: %-*.*s enumerator: %i6/0x%04x address: %p scale: %.3f",
	    lwrds,lwrds,std::get<1>(mlnk).c_str(),
	    (int)std::get<0>(mlnk),
	    (unsigned int)std::get<0>(mlnk),
	    (void*)std::get<2>(mlnk),
	    std::get<3>(mlnk));
    ATH_MSG_INFO( "Booked - " << buffer );
  }
  return !m_caloClusterMomentLink.empty();
}

bool ClusterTreeMaker::applyFilter(const xAOD::CaloCluster& rclus,xAOD::CaloCluster::State s) {
  // truth-based selection
  double edep(0.); if ( rclus.retrieveMoment(xAOD::CaloCluster::ENG_CALIB_TOT,edep) && (edep<m_clusterTruthEmin || edep>m_clusterTruthEmax) ) { return false; }
  // signal-based selection
  xAOD::CaloCluster::State scale = m_useLCWScaleAsReference ? xAOD::CaloCluster::CALIBRATED : s; 
  return ( rclus.e(scale) > m_clusterEmin && rclus.e(scale) < m_clusterEmax ) && ( rclus.eta(scale) > m_clusterRapMin && rclus.eta(scale) < m_clusterRapMax );
} 

bool ClusterTreeMaker::applyFilter(const xAOD::Jet& rjet) { 
  return rjet.pt() > m_jetPtMin && rjet.rapidity() > m_jetRapMin && rjet.rapidity() < m_jetRapMax;
}

ClusterTreeMaker::MatchedJetPairVector ClusterTreeMaker::findMatches(const xAOD::JetContainer& recoJets,const xAOD::JetContainer& truthJets) {
  // no truth or reco jets in container
  if ( truthJets.empty() || recoJets.empty() ) { return MatchedJetPairVector(); }
  // fill cross-lookup maps (reco->truth,truth->reco)
  RankedMatchedJetMap sortedRecoMap; RankedMatchedJetMap sortedTruthMap; double dR(0.); 
  for ( const auto* rj : recoJets ) {
    if ( this->applyFilter(*rj) ) { 
      for ( const auto* tj : truthJets ) { 
	dR = xAOD::P4Helpers::deltaR(rj,tj,true); 
	if ( dR < m_jetMatchRadius ) { 
	  sortedRecoMap .emplace_hint(sortedRecoMap .find(rj),rj,std::make_tuple(tj,dR));
	  sortedTruthMap.emplace_hint(sortedTruthMap.find(tj),tj,std::make_tuple(rj,dR));
	}
      } // loop truth jets
    } // reco jet passes selection
  } // loop reco jets
  // analyze matches
  MatchedJetPairVector matches;
  for ( auto fmap(sortedRecoMap.begin()); fmap != sortedRecoMap.end(); ++fmap ) {
    // check if exactly one match for given reco jet
    if ( sortedRecoMap.count(fmap->first) == 1 ) { 
      // check if exactly one truth jet match
      if ( sortedTruthMap.count(std::get<0>(fmap->second)) == 1 ) {
	matches.push_back( { fmap->first, std::get<0>(fmap->second), std::get<1>(fmap->second) } );
      } // truth jet has 1 match => 1-to-1 exclusive match
    } // reco jet has 1 match
  } // loop on reco jets
  return matches; 
}

bool ClusterTreeMaker::fillRecoJetBlock(const xAOD::Jet* pjet, xAOD::CaloCluster::State s /*,const xAOD::CaloClusterContainer& clusCont*/) {
  // calibrated jet scale 
  m_fJetCalE = pjet->e()/Gaudi::Units::GeV; m_fJetCalPt  = pjet->pt()/Gaudi::Units::GeV; m_fJetCalRap = pjet->rapidity(); m_fJetCalPhi = pjet->phi(); m_fJetNConst = pjet->numConstituents(); 
  // calculate raw energy scale and fill clusters in jets
  std::vector<const xAOD::IParticle*> partInJets = pjet->getConstituents().asIParticleVector();
  //  std::unique_ptr<ClusterVector> clusInJets(new ClusterVector()); clusInJets->reserve(std::max(partInJets.size(),size_t(100)));
  ClusterVector clusInJets; clusInJets.reserve(std::max(partInJets.size(),clusInJets.capacity()));
  xAOD::CaloCluster::GenVecFourMom_t clusP4(0.,0.,0.,0.); 
  if ( !partInJets.empty() ) {  
    for ( const auto* ppart : partInJets ) {
      const xAOD::CaloCluster* pclus = dynamic_cast<const xAOD::CaloCluster*>(ppart);       // cast to concrete object 
      if ( pclus != nullptr ) { 
	clusP4 += pclus->genvecP4(s);                                                       // get four-momentum at appropriate signal scale
	clusInJets.push_back(pclus);                                                        // collect valid constituents 
      }
    } 
  }
  m_fJetRawE   = clusP4.E() /Gaudi::Units::GeV;
  m_fJetRawPt  = clusP4.Pt()/Gaudi::Units::GeV;
  m_fJetRawRap = clusP4.Rapidity(); 
  m_fJetRawPhi = clusP4.Phi(); 
  ++m_fJetCount;
  // rank and filter
  return fillClusters<ClusterVector>(&clusInJets,m_fJetCalE,s);
}

bool ClusterTreeMaker::fillTruthJetBlock(const xAOD::Jet* pjet,double matchRadius) {
  if ( pjet == nullptr ) { ATH_MSG_ERROR( "invalid (null) pointer, expect valid pointer to truth jet object" ); return false; } 
  m_fTruthJetE           = pjet->e() /Gaudi::Units::GeV;
  m_fTruthJetPt          = pjet->pt()/Gaudi::Units::GeV; 
  m_fTruthJetRap         = pjet->rapidity(); 
  m_fTruthJetPhi         = pjet->phi();
  m_fTruthJetMatchRadius = matchRadius; 
  return true;
}

bool ClusterTreeMaker::resetAll() {
  // -- event info
  m_runNumber   = 0;
  m_eventNumber = 0;
  m_nPrimVtx    = 0;
  m_averageMu   = 0.;
  // -- single particles
  if ( m_useSingleParticles ) {
    // truth info
    m_fClusterTruthE   = 0.;
    m_fClusterTruthPt  = 0.;
    m_fClusterTruthEta = 0.;
    m_fClusterTruthPhi = 0.;
    m_fClusterTruthPDG = 0 ;
  }
  // -- jets
  if ( m_useJets ) { 
    // reco jet info (calibrated)
    m_fJetCalE         = -1.;
    m_fJetCalPt        =  0.;
    m_fJetCalRap       =  0.;
    m_fJetCalPhi       =  0.;
    m_fJetNConst       =  0 ;
    // m_fJetCount        =  0 ;
    // reco jet info (constituent scale)
    m_fJetRawE         = -1.;
    m_fJetRawPt        =  0.;
    m_fJetRawRap       =  0.;
    m_fJetRawPhi       =  0.; 
    // truth jet info
    m_fTruthJetMatchRadius  = -1.;
    m_fTruthJetE            = -1.;
    m_fTruthJetPt           =  0.;
    m_fTruthJetRap          =  0.;
    m_fTruthJetPhi          =  0.;
  }
  // -- cluster specs
  m_nCluster                = 0;
  m_fClusterIndex           = 0;
  // -- reset clusters
  return resetClusters();
}
bool ClusterTreeMaker::resetClusters() {
  // -- cluster specs
  m_fCluster_nCells     = 0; // # cells with E > 0
  m_fCluster_nCells_tot = 0; // # all cells
  // -- cluster signals: E0 scale (optional use)
  m_fClusterE          = 0.;
  m_fClusterPt         = 0.;
  m_fClusterEta        = 0.;
  m_fClusterPhi        = 0.;
  m_fCluster_sumCellE  = 0.;
  m_fClusterTime       = 0.;
  m_fCluster_fracE     = 0.; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  m_fCluster_fracE_ref = 0.; // fractional contribution particle (cluster_energy/reference energy)   
  // -- cluster signals: LCW scale (default/always use)
  m_fClusterECalib          = 0.;
  m_fClusterPtCalib         = 0.;
  m_fClusterEtaCalib        = 0.;
  m_fClusterPhiCalib        = 0.;
  m_fCluster_sumCellECalib  = 0.;
  m_fCluster_fracECalib     = 0.; // fractional contribution detector (cluster_energy/sum_cluster_energy)
  m_fCluster_fracECalib_ref = 0.; // fractional contribution particle (cluster_energyreference energy)   
  // -- cluster truth info
  m_fCluster_ENG_CALIB_TOT       = 0.;   // energy deposit in cluster cells 
  m_fCluster_ENG_CALIB_OUT_T     = 0.;   // energy deposit outside of cluster but associated (tight)
  m_fCluster_ENG_CALIB_OUT_L     = 0.;   // energy deposit outside of cluster but associated (loose)
  m_fCluster_ENG_CALIB_OUT_M     = 0.;   // energy deposit outside of cluster but associated (medium)
  m_fCluster_ENG_CALIB_DEAD_T    = 0.;   // dead material energy deposit associated with cluster (tight)
  m_fCluster_ENG_CALIB_DEAD_L    = 0.;   // dead material energy deposit associated with cluster (loose)
  m_fCluster_ENG_CALIB_DEAD_M    = 0.;   // dead material energy deposit associated with cluster (medium)
  m_fCluster_ENG_CALIB_DEAD_TOT  = 0.;   // dead material energy deposit associated with cluster (all)
  m_fCluster_ENG_CALIB_FRAC_EM   = 0.;   // energy deposited by electromagnetic processes
  m_fCluster_ENG_CALIB_FRAC_HAD  = 0.;   // energy depoisted by hadronic processes/ionizations
  m_fCluster_ENG_CALIB_FRAC_REST = 0.;   // energy deposited by all other processes (unspecific in Geant4)
  // -- calibration weights
  m_fCluster_EM_PROBABILITY = 0.;       // EM likelihood
  m_fCluster_HAD_WEIGHT     = 0.;       // hadronic weights sum(w_cell x E_cell)/sum(E_cell)
  m_fCluster_OOC_WEIGHT     = 0.;       // out-of-cluster correction factor (OOC)
  m_fCluster_DM_WEIGHT      = 0.;       // dead material correction factor (DMC)
  m_fCluster_Ehad           = 0.;       // hadronic calibration applied:  HAD_WEIGHT * ClusterE
  m_fCluster_Eooc           = 0.;       // amount of energy added by OOC: (1-OOC_WEIGHT)*Ehad 
  m_fCluster_Edmc           = 0.;       // amount of energy added by DMC: (1-DM_WEIGHT)*(Ehad+Eooc)
  // -- cluster moments (used as configured)
  m_fCluster_CENTER_MAG        = 0.;    // always used (historic)
  m_fCluster_FIRST_PHI         = 0.;
  m_fCluster_FIRST_ETA         = 0.;
  m_fCluster_SECOND_R          = 0.;
  m_fCluster_SECOND_LAMBDA     = 0.;
  m_fCluster_DELTA_PHI         = 0.;
  m_fCluster_DELTA_THETA       = 0.;
  m_fCluster_DELTA_ALPHA       = 0.;
  m_fCluster_CENTER_X          = 0.;
  m_fCluster_CENTER_Y          = 0.;
  m_fCluster_CENTER_Z          = 0.;
  m_fCluster_CENTER_LAMBDA     = 0.;
  m_fCluster_LATERAL           = 0.;
  m_fCluster_LONGITUDINAL      = 0.;
  m_fCluster_ENG_FRAC_EM       = 0.;
  m_fCluster_ENG_FRAC_MAX      = 0.;
  m_fCluster_ENG_FRAC_CORE     = 0.;
  m_fCluster_FIRST_ENG_DENS    = 0.;    // always used (historic)
  m_fCluster_SECOND_ENG_DENS   = 0.;
  m_fCluster_ISOLATION         = 0.;
  m_fCluster_ENG_BAD_CELLS     = 0.;
  m_fCluster_N_BAD_CELLS       = 0.;
  m_fCluster_N_BAD_CELLS_CORR  = 0.;
  m_fCluster_BAD_CELLS_CORR_E  = 0.;
  m_fCluster_BADLARQ_FRAC      = 0.;
  m_fCluster_ENG_POS           = 0.;
  m_fCluster_SIGNIFICANCE      = 0.;
  m_fCluster_CELL_SIGNIFICANCE = 0.;
  m_fCluster_CELL_SIG_SAMPLING = 0.;
  m_fCluster_AVG_LAR_Q         = 0.;
  m_fCluster_AVG_TILE_Q        = 0.;
  m_fCluster_ENG_BAD_HV_CELLS  = 0.;
  m_fCluster_N_BAD_HV_CELLS    = 0.;
  m_fCluster_PTD               = 0.;
  m_fCluster_MASS              = 0.;
  m_fCluster_SECOND_TIME       = 0.;

  return true; 
}

void ClusterTreeMaker::debugMessage(const std::string& module,const std::string& fmtStr,...) {
  // some constants
  static constexpr int                    nsize  = 2048; 
  static const     std::string::size_type swidth = CTM_MNAME_W ;
  static const     std::string            dots   = "...";
  // static constexpr std::string            lbrak  = "[";
  // static constexpr std::string            rbrak  = "]";
  // buffer
  char buffer[nsize];
  // -- check if debug stream is active
  if ( !m_debugStreamFlag || !m_debugStream.good() ) { return; }
  // -- adapt string for display
  std::string modn(module);
  if ( modn.length() > swidth  ) { modn.resize(swidth-dots.length()); modn += dots; }
  int npos(sprintf(&buffer[0],"[%-*.*s][%6i] ",(int)swidth,(int)swidth,modn.c_str(),m_allEvntCtr));
  // add variable argument list to displayed string
  va_list args; 
  va_start(args,fmtStr); 
  npos += vsnprintf(&buffer[npos],nsize,fmtStr.c_str(),args); 
  va_end(args);
  // print message and flush (for debugging)
  m_debugStream << buffer << std::endl << std::flush;
}

void ClusterTreeMaker::resourceMessage(const std::string& module,const std::string& tag) { 
  static const int nsize = 512;
  static char buffer[nsize];
  sprintf(buffer,"%s --> |- vmem %s -|- pmem %s -|",tag.c_str(),Resource::virtualMemory(Resource::Unit::kB,true).c_str(),Resource::physicalMemory(Resource::Unit::kB,true).c_str());
  debugMessage(module,std::string(buffer));
}
