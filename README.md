# MLTree package

## How to setup

```
mkdir MLTreeMaker
cd MLTreeMaker
setupATLAS -c centos7
lsetup git
git atlas init-workdir ssh://git@gitlab.cern.ch:7999/loch/athena.git -b SingleParticles -g loch
```
Just one time, if you have any error to compile - I forgot what was the error I saw... This part may not be necessary for you. 
```
scl enable devtoolset-7 bash
cmake -DATLAS_PACKAGE_FILTER_FILE=../package_filters.txt ../athena/Projects/WorkDir -DCMAKE_CXX_COMPILER=`which g++` -DCMAKE_C_COMPILER=`which gcc`

```

from `MLTreeMaker/athena` folder,
```
cd athena
git atlas addpkg xAODCaloEvent
git atlas addpkg CaloLumiConditions
git atlas addpkg CaloRec

cd ..

mkdir build
cd build
asetup Athena,22.0.28,here

cmake -DATLAS_PACKAGE_FILTER_FILE=../athena/Event/xAOD/xAODCaloEven ../athena/Calorimeter/CaloLumiConditions ../athena/Calorimeter/CaloRec ../package_filters.txt  ../athena/Projects/WorkDir
make

cd ..
git clone ssh://git@gitlab.cern.ch:7999/doyeong/mltree.git athena/MLTree
cd -

cmake -DATLAS_PACKAGE_FILTER_FILE=../athena/Event/xAOD/xAODCaloEven ../athena/Calorimeter/CaloLumiConditions ../athena/Calorimeter/CaloRec ../package_filters.txt  ../athena/MLTree  ../athena/Projects/WorkDir

make
source x86_64-centos7-gcc8-opt/setup.sh

```

## Test run
from `MLTreeMaker` folder,
```
mkdir run; cd run
athena.py ../athena/MLTree/python/runargs.ESDtoAOD.AllClusters.Cells.py RecJobTransforms/skeleton.ESDtoAOD_tf.py
```
